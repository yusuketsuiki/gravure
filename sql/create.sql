create table idols
(
    id int primary key auto_increment,
    name varchar(255),
    picture_num int,
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
    
);

create table users
(
    id int primary key auto_increment,
    owner varchar(255),
    name varchar(255),
    photosurl varchar(255),
    profileurl varchar(255),
    mobileurl varchar(255),
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
    
);

drop table photos;
create table photos
(
    id int primary key auto_increment,
    idol_id int,
    user_id int,
    photo_id varchar(255),
    secret varchar(255),
    farm varchar(255),
    server varchar(255),
    name varchar(255),
    is_tag varchar(1),
    is_text varchar(1),
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
);

alter table photos add delete_flag varchar(1) default "1" after is_text;


create table sites
(
    id int primary key auto_increment,
    name varchar(255),
    site_name varchar(255),
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
);

create table groups
(
    id int primary key auto_increment,
    site_id int,
    name varchar(255),
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
);

create table admins
(
    id int primary key auto_increment,
    name varchar(255),
    password varchar(255),
    rkey varchar(255),
    delete_flag varchar(1) default '0',
    created_at datetime default '0000-00-00 00:00:00',
    updated_at datetime default '0000-00-00 00:00:00'
);



alter table idols add site_id int after id;
alter table idols add group_id int after site_id;

update idols set site_id = 1;

insert into sites (name, site_name) value ('gravure','グラビアアイドル');
insert into sites (name, site_name) value ('akb','AKBグループ');
insert into sites (name, site_name) value ('3bjr','スリービージュニア');

insert into groups (site_id, name) value (2, 'AKB48');
insert into groups (site_id, name) value (2, 'SKE48');
insert into groups (site_id, name) value (2, 'NMB48');
insert into groups (site_id, name) value (2, 'HKT48');

insert into groups (site_id, name) value (3, 'ももいろクローバーZ');
insert into groups (site_id, name) value (3, '私立恵比寿中学');
insert into groups (site_id, name) value (3, 'チームしゃちほこ');
insert into groups (site_id, name) value (3, 'たこやきレインボー');
insert into groups (site_id, name) value (3, 'KAGAJO☆4S');
insert into groups (site_id, name) value (3, 'みにちあ☆ベアーズ');

insert into idols (site_id, group_id, name) values (3,1, '百田夏菜子');
insert into idols (site_id, group_id, name) values (3,1, '玉井詩織');
insert into idols (site_id, group_id, name) values (3,1, '佐々木彩夏');
insert into idols (site_id, group_id, name) values (3,1, '有安杏果');
insert into idols (site_id, group_id, name) values (3,1, '高城れに');

insert into idols (site_id, group_id, name) values (3,2, '瑞季');
insert into idols (site_id, group_id, name) values (3,2, '真山りか');
insert into idols (site_id, group_id, name) values (3,2, '杏野なつ');
insert into idols (site_id, group_id, name) values (3,2, '安本彩花');
insert into idols (site_id, group_id, name) values (3,2, '廣田あいか');
insert into idols (site_id, group_id, name) values (3,2, '星名美怜');
insert into idols (site_id, group_id, name) values (3,2, '鈴木裕乃');
insert into idols (site_id, group_id, name) values (3,2, '松野莉奈');
insert into idols (site_id, group_id, name) values (3,2, '柏木ひなた');
insert into idols (site_id, group_id, name) values (3,2, '小林歌穂');
insert into idols (site_id, group_id, name) values (3,2, '中山莉子');

insert into idols (site_id, group_id, name) values (3,3, '秋本帆華');
insert into idols (site_id, group_id, name) values (3,3, '咲良菜緒');
insert into idols (site_id, group_id, name) values (3,3, '安藤ゆず');
insert into idols (site_id, group_id, name) values (3,3, '大黒柚姫');
insert into idols (site_id, group_id, name) values (3,3, '坂本遥奈');
insert into idols (site_id, group_id, name) values (3,3, '伊藤千由李');

insert into idols (site_id, group_id, name) values (3,4, '奈良崎とわ');
insert into idols (site_id, group_id, name) values (3,4, '彩木咲良');
insert into idols (site_id, group_id, name) values (3,4, '堀くるみ');
insert into idols (site_id, group_id, name) values (3,4, '春名真依');
insert into idols (site_id, group_id, name) values (3,4, '清井咲希');

insert into idols (site_id, group_id, name) values (3,5, '奥澤レイナ');
insert into idols (site_id, group_id, name) values (3,5, '小田桐汐里');
insert into idols (site_id, group_id, name) values (3,5, '藤本杏');
insert into idols (site_id, group_id, name) values (3,5, '内山あみ');

insert into idols (site_id, group_id, name) values (3,6, '斎藤夏鈴');
insert into idols (site_id, group_id, name) values (3,6, '内藤るな');
insert into idols (site_id, group_id, name) values (3,6, '國光真央');
insert into idols (site_id, group_id, name) values (3,6, '芽奈');
insert into idols (site_id, group_id, name) values (3,6, '椎名るか');
insert into idols (site_id, group_id, name) values (3,6, '巨勢紫');
insert into idols (site_id, group_id, name) values (3,6, '高井千帆');
insert into idols (site_id, group_id, name) values (3,6, '小田垣陽菜');
insert into idols (site_id, group_id, name) values (3,6, '平瀬美里');
insert into idols (site_id, group_id, name) values (3,6, '高見真愛');
insert into idols (site_id, group_id, name) values (3,6, '愛来');
insert into idols (site_id, group_id, name) values (3,6, '石黒詩苑');


insert into idols (site_id, group_id, name) values (2,null, '伊豆田莉奈');
insert into idols (site_id, group_id, name) values (2,null, '入山杏奈');
insert into idols (site_id, group_id, name) values (2,null, '岩田華怜');
insert into idols (site_id, group_id, name) values (2,null, '大島涼花');
insert into idols (site_id, group_id, name) values (2,null, '川栄李奈');
insert into idols (site_id, group_id, name) values (2,null, '菊地あやか');
insert into idols (site_id, group_id, name) values (2,null, '兒玉遥');
insert into idols (site_id, group_id, name) values (2,null, '小林茉里奈');
insert into idols (site_id, group_id, name) values (2,null, '佐々木優佳里');
insert into idols (site_id, group_id, name) values (2,null, '佐藤すみれ');
insert into idols (site_id, group_id, name) values (2,null, '鈴木まりや');
insert into idols (site_id, group_id, name) values (2,null, '高橋朱里');
insert into idols (site_id, group_id, name) values (2,null, '高橋みなみ');
insert into idols (site_id, group_id, name) values (2,null, '田野優花');
insert into idols (site_id, group_id, name) values (2,null, '松井咲子');
insert into idols (site_id, group_id, name) values (2,null, '森川彩香');
insert into idols (site_id, group_id, name) values (2,null, '矢倉楓子');
insert into idols (site_id, group_id, name) values (2,null, '横山由依');
insert into idols (site_id, group_id, name) values (2,null, '渡辺麻友');
insert into idols (site_id, group_id, name) values (2,null, '田北香世子');
insert into idols (site_id, group_id, name) values (2,null, '西山怜那');
insert into idols (site_id, group_id, name) values (2,null, '阿部マリア');
insert into idols (site_id, group_id, name) values (2,null, '内田眞由美');
insert into idols (site_id, group_id, name) values (2,null, '大島優子');
insert into idols (site_id, group_id, name) values (2,null, '北原里英');
insert into idols (site_id, group_id, name) values (2,null, '倉持明日香');
insert into idols (site_id, group_id, name) values (2,null, '小林香菜');
insert into idols (site_id, group_id, name) values (2,null, '佐藤亜美菜');
insert into idols (site_id, group_id, name) values (2,null, '島田晴香');
insert into idols (site_id, group_id, name) values (2,null, '鈴木紫帆里');
insert into idols (site_id, group_id, name) values (2,null, '近野莉菜');
insert into idols (site_id, group_id, name) values (2,null, '永尾まりや');
insert into idols (site_id, group_id, name) values (2,null, '中田ちさと');
insert into idols (site_id, group_id, name) values (2,null, '野澤玲奈');
insert into idols (site_id, group_id, name) values (2,null, '平田梨奈');
insert into idols (site_id, group_id, name) values (2,null, '藤田奈那');
insert into idols (site_id, group_id, name) values (2,null, '古畑奈和');
insert into idols (site_id, group_id, name) values (2,null, '前田亜美');
insert into idols (site_id, group_id, name) values (2,null, '松井珠理奈');
insert into idols (site_id, group_id, name) values (2,null, '宮崎美穂');
insert into idols (site_id, group_id, name) values (2,null, '武藤十夢');
insert into idols (site_id, group_id, name) values (2,null, '後藤萌咲');
insert into idols (site_id, group_id, name) values (2,null, '下口ひなな');
insert into idols (site_id, group_id, name) values (2,null, '石田晴香');
insert into idols (site_id, group_id, name) values (2,null, '市川美織');
insert into idols (site_id, group_id, name) values (2,null, '岩佐美咲');
insert into idols (site_id, group_id, name) values (2,null, '梅田彩佳');
insert into idols (site_id, group_id, name) values (2,null, '大場美奈');
insert into idols (site_id, group_id, name) values (2,null, '大森美優');
insert into idols (site_id, group_id, name) values (2,null, '大家志津香');
insert into idols (site_id, group_id, name) values (2,null, '柏木由紀');
insert into idols (site_id, group_id, name) values (2,null, '片山陽加');
insert into idols (site_id, group_id, name) values (2,null, '加藤玲奈');
insert into idols (site_id, group_id, name) values (2,null, '小嶋菜月');
insert into idols (site_id, group_id, name) values (2,null, '小嶋陽菜');
insert into idols (site_id, group_id, name) values (2,null, '島崎遥香');
insert into idols (site_id, group_id, name) values (2,null, '高城亜樹');
insert into idols (site_id, group_id, name) values (2,null, '竹内美宥');
insert into idols (site_id, group_id, name) values (2,null, '田名部生来');
insert into idols (site_id, group_id, name) values (2,null, '中村麻里子');
insert into idols (site_id, group_id, name) values (2,null, '名取稚菜');
insert into idols (site_id, group_id, name) values (2,null, '野中美郷');
insert into idols (site_id, group_id, name) values (2,null, '藤江れいな');
insert into idols (site_id, group_id, name) values (2,null, '山内鈴蘭');
insert into idols (site_id, group_id, name) values (2,null, '渡辺美優紀');
insert into idols (site_id, group_id, name) values (2,null, '川本紗矢');
insert into idols (site_id, group_id, name) values (2,null, '横島亜衿');
insert into idols (site_id, group_id, name) values (2,null, '岩立沙穂');
insert into idols (site_id, group_id, name) values (2,null, '内山奈月');
insert into idols (site_id, group_id, name) values (2,null, '梅田綾乃');
insert into idols (site_id, group_id, name) values (2,null, '岡田彩花');
insert into idols (site_id, group_id, name) values (2,null, '岡田奈々');
insert into idols (site_id, group_id, name) values (2,null, '北澤早紀');
insert into idols (site_id, group_id, name) values (2,null, '小嶋真子');
insert into idols (site_id, group_id, name) values (2,null, '篠崎彩奈');
insert into idols (site_id, group_id, name) values (2,null, '髙島祐利奈');
insert into idols (site_id, group_id, name) values (2,null, '西野未姫');
insert into idols (site_id, group_id, name) values (2,null, '橋本耀');
insert into idols (site_id, group_id, name) values (2,null, '前田美月');
insert into idols (site_id, group_id, name) values (2,null, '村山彩希');
insert into idols (site_id, group_id, name) values (2,null, '茂木忍');
