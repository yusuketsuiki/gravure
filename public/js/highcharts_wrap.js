document.write('<script type="text/javascript" language="JavaScript" src="/js//highcharts.js"></script>');

//-------------------------------
// 円グラフの作成
//-------------------------------
function draw_pie(area,data) {
    // グラフオプションを指定
    var options =
    {
        chart :{
            renderTo : area,
            type: "pie",
            backgroundColor:"#fff",
        },

        title: {text: data["title"]},
        tooltip: {valueSuffix: data["tooltip"]},
        plotOptions: {
            pie: {
                dataLabels: {
                     formatter: function() {
                         return '<b>'+ this.point.name +'</b>:'+ Math.round(this.percentage*10)/10 + '%';
                    }
                }
            }
        },

        // データ系列を作成
        series: [
            {
                type: 'pie',
                name: data["text"], 
                data: data["data"],
            }
        ]
    }
    
    // グラフを作成
    chart = new Highcharts.Chart(options);
}


function draw_chart(area,data) {
    // グラフオプションを指定
    var options =
    {
        chart :{
            renderTo : area,
            type:"column",
            backgroundColor:"#fff",
        },

        title : {text: data["title"]},
        xAxis : {
            categories: data["xcategories"],
            title: {text: data["xtext"]}
        },
        yAxis : {
            min:0,
            max:5,
            title: {text: data["ytext"]}
        },
        series: [
          {name: data["tooltip"], data: data["data"]},
        ]
    }
    
    // グラフを作成
    chart = new Highcharts.Chart(options);
}