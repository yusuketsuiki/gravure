<?php
return array(
    'required' => ':labelを入力してください。',
    'max_length' => ':labelは:param:1文字以内で入力してください。',
    'min_length' => ':labelは:param:1文字以上で入力してください。',
    'numeric_min' => ':labelは半角数字で入力してください。',
    'valid_email' => ':labelの書式が正しくありません。',
    'valid_url' => ':labelの書式が正しくありません。',
    'kana' => ':labelは全角カタカナで入力してください。',
);
