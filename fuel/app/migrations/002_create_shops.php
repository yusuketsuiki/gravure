<?php

namespace Fuel\Migrations;

class Create_shops
{
	public function up()
	{
		\DBUtil::create_table('shops', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'area_id' => array('constraint' => 11, 'type' => 'int'),
			'tdhk_id' => array('constraint' => 11, 'type' => 'int'),
			'image_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'address' => array('constraint' => 255, 'type' => 'varchar'),
			'access' => array('type' => 'text'),
			'tel' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
			'fax' => array('constraint' => 20, 'type' => 'varchar', 'null' => true),
			'open_time' => array('constraint' => 255, 'type' => 'varchar'),
			'holiday' => array('constraint' => 255, 'type' => 'varchar'),
			'mail' => array('constraint' => 255, 'type' => 'varchar'),
			'url' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'capacity' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'is_car' => array('constraint' => 1, 'type' => 'varchar', 'default' => '0'),
			'equipment' => array('type' => 'text', 'null' => true),
			'etc' => array('type' => 'text', 'null' => true),
			'delete_flag' => array('constraint' => 1, 'type' => 'varchar', 'default' => '0'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('shops');
	}
}