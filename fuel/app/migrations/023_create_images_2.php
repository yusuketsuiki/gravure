<?php

namespace Fuel\Migrations;

class Create_images_2
{
	public function up()
	{
		\DBUtil::create_table('images_2', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'path' => array('constraint' => 255, 'type' => 'varchar'),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'extension' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('images_2');
	}
}