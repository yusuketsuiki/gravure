<?php

namespace Fuel\Migrations;

class Create_images
{
	public function up()
	{
		\DBUtil::create_table('images', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'path1' => array('constraint' => 255, 'type' => 'varchar'),
			'url1' => array('constraint' => 255, 'type' => 'varchar'),
			'path2' => array('constraint' => 255, 'type' => 'varchar'),
			'url2' => array('constraint' => 255, 'type' => 'varchar'),
			'path3' => array('constraint' => 255, 'type' => 'varchar'),
			'url3' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('images');
	}
}