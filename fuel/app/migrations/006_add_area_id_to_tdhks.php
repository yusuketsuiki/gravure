<?php

namespace Fuel\Migrations;

class Add_area_id_to_tdhks
{
	public function up()
	{
		\DBUtil::add_fields('tdhks', array(
			'area_id' => array('constraint' => 11, 'type' => 'int'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('tdhks', array(
			'area_id'

		));
	}
}