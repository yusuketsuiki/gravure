<?php

namespace Fuel\Migrations;

class Create_tdhks_2
{
	public function up()
	{
		\DBUtil::create_table('tdhks_2', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('tdhks_2');
	}
}