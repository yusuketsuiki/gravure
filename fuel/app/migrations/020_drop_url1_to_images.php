<?php

namespace Fuel\Migrations;

class Drop_url1_to_images
{
	public function up()
	{
		\DBUtil::drop_table('images');
	}

	public function down()
	{
		\DBUtil::create_table('images', array(
			'id' => array('type' => 'int', 'null' => true, 'constraint' => 11, 'auto_increment' => true),
			'path1' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'url1' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'path2' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'url2' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'path3' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'url3' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),
			'extension' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'path' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),
			'name' => array('type' => 'varchar', 'null' => true, 'constraint' => 255),

		), array('id'));

	}
}