<?php

namespace Fuel\Migrations;

class Create_times
{
	public function up()
	{
		\DBUtil::create_table('times', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'shop_id' => array('constraint' => 11, 'type' => 'int'),
			'start' => array('constraint' => 4, 'type' => 'varchar'),
			'end' => array('constraint' => 4, 'type' => 'varchar'),
			'money' => array('constraint' => 11, 'type' => 'int'),
			'delete_flag' => array('constraint' => 1, 'type' => 'varchar', 'default' => '0'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('times');
	}
}