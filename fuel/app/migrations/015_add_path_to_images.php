<?php

namespace Fuel\Migrations;

class Add_path_to_images
{
	public function up()
	{
		\DBUtil::add_fields('images', array(
			'path' => array('constraint' => 255, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('images', array(
			'path'

		));
	}
}