<?php

namespace Fuel\Migrations;

class Add_name_to_images
{
	public function up()
	{
		\DBUtil::add_fields('images', array(
			'name' => array('constraint' => 255, 'type' => 'varchar'),

		));
	}

	public function down()
	{
		\DBUtil::drop_fields('images', array(
			'name'

		));
	}
}