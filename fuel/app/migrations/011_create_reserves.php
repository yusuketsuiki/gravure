<?php

namespace Fuel\Migrations;

class Create_reserves
{
	public function up()
	{
		\DBUtil::create_table('reserves', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true),
			'shop_id' => array('constraint' => 11, 'type' => 'int'),
			'time_id' => array('constraint' => 11, 'type' => 'int'),
			'date' => array('constraint' => 8, 'type' => 'varchar'),
			'name' => array('constraint' => 80, 'type' => 'varchar'),
			'delete_flag' => array('constraint' => 1, 'type' => 'varchar', 'default' => '0'),
			'created_at' => array('type' => 'timestamp', 'null' => true),
			'updated_at' => array('type' => 'timestamp', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('reserves');
	}
}