<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Fuel\Tasks;

/**
 * Yahoo映画のURLから評価を取得する
 *
 * @package		Fuel
 * @version		1.0
 * @author		Yusuke Tsuiki
 */

class Docal
{
	public static function run($speech = null)
	{
        $movies = \Model_Movie::find()->get();
        
        foreach ($movies as $movie)
        {
            echo $movie->id."\n";
            $movie->doCalculation();
        }
	}
}

/* End of file tasks/robots.php */
