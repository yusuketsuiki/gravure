<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Fuel\Tasks;

/**
 * Yahoo映画のURLから評価を取得する
 *
 * @package		Fuel
 * @version		1.0
 * @author		Yusuke Tsuiki
 */

class Getidols
{
	public static function run($speech = null)
	{
        // アイドルセクシー画像集
        $html = file_get_contents("http://intervalues.com/idol.html");
        $html = str_replace("\n","",$html);
        $html = str_replace("\r","",$html);
        $html = str_replace("\r\n","",$html);
        $html = mb_convert_encoding($html, "utf-8","shift_jis");
        if(preg_match("/あ行のアイドル・グラビアアイドル・女優の画像(.+?)裏<BR>A/",$html,$match))
        {
            if(preg_match_all("/<a href=\"http:\/\/inter.+?_blank\">(.+?)</",$match[1],$match2))
            {
                foreach ($match2[1] as $name)
                {
                    $idol = \Model_Idol::find()->where("name","=",$name)->get_one();
                    if (!($idol instanceof \Model_Idol))
                    {
                        $idol = new \Model_Idol();
                        $idol->name = $name;
                        $idol->picture_num = 0;
                        $idol->save();
                    }
                }
            }
        }
	}
}

/* End of file tasks/robots.php */
