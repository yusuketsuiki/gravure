<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

namespace Fuel\Tasks;

/**
 * 画像一覧をFlickrから取得
 *
 * @package		Fuel
 * @version		1.0
 * @author		Yusuke Tsuiki
 */

class Search
{
	public static function run($speech = null)
	{
        // アイドル一覧を取得
        //$idols = \Model_Idol::find()->get();
        $idols = \Model_Idol::find()->where("site_id","=",2)->order_by("updated_at","asc")->limit(10)->get();
        
        foreach ($idols as $idol)
        {
            $list = \myFlickr::photosSearch($idol);
        }
	}
}

/* End of file tasks/robots.php */
