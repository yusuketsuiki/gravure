<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**v 
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Frontend_Home_Index extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	public function view()
	{
        // サイトIDに対応したアイドルのみ表示
        $idol_list = Model_Idol::find()->where("site_id","=",$this->site_id)->get();
        $idol_id_in = array();
        foreach ($idol_list as $idol)
        {
            $idol_id_in[] = $idol->id;
        }
        
        // 各アイドルに対して画像を一枚取得
        $photo_list = Model_Photo::find()->where("idol_id","in",$idol_id_in)->order_by(DB::expr('RAND()'))->group_by("idol_id")->get();
        $this->photo_list = $photo_list;
    }
}
