<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Frontend_Idol_Index extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
    public function view()
    {
        // 画像一覧を取得
        $orm = Model_Photo::find()->where("idol_id","=",$this->idol->id);
        
        // ページャ
        $option = array();
        $disp = 24;
        $total = $orm->count();
        $pager = new Pager($total, '/idol/'.$this->idol->id,$disp,$option);
        $orm->limit($disp)->offset($pager->getOffset());
        
        // 表示
        $this->pager = $pager;
        $this->photo_list = $orm->get();
    }
}
