<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Frontend_Movie_Review extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
    public function view()
    {
        $mode = $this->mode;
    
        // 元データを取得
        $orm = Model_Movieuser::find()->where('movie_id','=',$this->id);
        if ($mode != 'A')
        {
            $orm->where('point','=',$mode);
        }
        $movieusers = $orm->get();

        //---------------------------------
        // 得点ごとのレビュワー属性分布
        //---------------------------------
        // 項目定義
        $column_hash = array(
            "100" => "マニア",
            "10" => "ファン",
            "2" => "一般ユーザ",
            "0" => "一見さん",
            "X" => "非公開ユーザ",
        );
        
        // レビュアー属性の比率
        $title = '全投稿のレビュアーの分布';
        if ($mode != "A")
        {
            $title = $mode."点を投稿しているレビュアーの分布";
        }
        
        $review_graff['title'] = $title;
        $review_graff['tooltip'] = '人';
        $review_graff['text'] = 'レビュアー数';
        
        // データを取得
        $review_data = array();
        $avg_data = array();
        
        foreach ($column_hash as $key => $value)
        {
            $review_data[$value] = 0;
        }

        // データを取得する
        foreach ($movieusers as $movieuser)
        {
            // レビュー数を公開しているか
            if ($movieuser->user->prof_flag == "0")
            {
                foreach ($column_hash as $num => $value)
                {
                    if ($movieuser->user->review_num >= $num)
                    {
                        $review_data[$value]++;
                        break;
                    }
                }
            }
            else
            {
                $review_data[$column_hash['X']]++;
            }
        }
        
        foreach ($review_data as $name => $num)
        {
            $review_graff['data'][] = array($name, $num);
        }

        // 表示
        $this->json = json_encode($review_graff);
    }
}
