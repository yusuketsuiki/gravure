<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Frontend_Movie_Point extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
    public function view()
    {
        $mode = $this->mode;

        $column_hash = array(
            "A" => "全てのユーザ",
            "100" => "マニア",
            "10" => "ファン",
            "2" => "一般ユーザ",
            "0" => "一見さん",
            "X" => "非公開ユーザ",
            "X0" => '一見さん＋レビュー数非公開',
        );
    
        // 元データを取得
        $orm = Model_Movieuser::find()->where('movie_id','=',$this->id);
        
        if ($mode == 'X')
        {
            $orm->related('user')->where('user.prof_flag','=','X');
        }
        elseif (($mode != 'A') && ($mode != 'X0'))
        {
            $orm->related('user')->where('user.review_num','>=',$mode);
        }
        
        $movieusers = $orm->get();

        // 単発＋レビュー数非公開
        if ($mode == 'X0')
        {
            $tmp = $movieusers;
            $movieusers = array();
            foreach ($tmp as $movieuser)
            {
                if (($movieuser->user->review_num <= 1) || ($movieuser->user->prof_flag == 'X'))
                {
                    $movieusers[] = $movieuser;
                }
            }
        }

        //---------------------------------
        // レビュワー属性ごとの得点分布
        //---------------------------------
        // レビュアー属性の比率
        $ret['title'] = $column_hash[$mode].'が投稿した得点の分布';
        $ret['tooltip'] = '人';
        $ret['text'] = 'レビュアー数';
        
        // データを取得
        $data = array('5点' => 0,'4点' => 0,'3点' => 0,'2点' => 0,'1点' => 0);

        // データを取得する
        foreach ($movieusers as $movieuser)
        {
            $data[$movieuser->point."点"]++;
        }
        
        foreach ($data as $key => $value)
        {
            $ret['data'][] = array($key, $value);
        }

        // 表示
        $this->json = json_encode($ret);
    }
}
