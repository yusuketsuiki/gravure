<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Frontend_Api_City extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
    public function view()
    {
        $city_list = Model_City::find()->where("tdhk_id","=",$this->id)->get();
        $select_city_list = array("" => "選択してください");
        
        foreach ($city_list as $city)
        {
            $select_city_list[$city->id] = $city->name;
        }
        
        $this->select_city_list = $select_city_list;
    }
}
