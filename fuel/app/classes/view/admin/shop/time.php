<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The welcome hello view model.
 *
 * @package  app
 * @extends  ViewModel
 */
class View_Admin_Shop_Time extends ViewModel
{
	/**
	 * Prepare the view data, keeping this in here helps clean up
	 * the controller.
	 *
	 * @return void
	 */
	public function view()
	{
        // 登録済の時間帯を取得
        $times = Model_Time::find()->where('shop_id',"=",$this->shop->id)->where('delete_flag','=','0')->order_by('youbi')->order_by('date')->order_by('start')->get();

        $this->times = $times;
        $this->ymd_list = myUtil::getYmdList('',date('Y'),date('Y', strtotime("+1 year")));
        $this->youbi_list = array(
            '1' => '日曜日',
            '2' => '月曜日',
            '3' => '火曜日',
            '4' => '水曜日',
            '5' => '木曜日',
            '6' => '金曜日',
            '7' => '土曜日',
        );
    }
}
