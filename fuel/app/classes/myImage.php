<?php
class MyImage
{
    private static $base_path =  '../../public/uploads/';
    private static $base_url =  '/uploads/';
    
	/*
	 * 確認時の処理
	 */
	public static function doConfirm($file_path, $sizes = array())
	{
        $base_path = APPPATH . self::$base_path;
        $path = $base_path.$file_path;
        $filedata = array();

        // サイズの設定
        $sizes = array(
            '1' => array('w' => 480, 'h' => 320),
            '2' => array('w' => 240, 'h' => 160),
            't' => array('w' => 180, 'h' => 180),
        );
    
        // アップロード処理
        Upload::process(array('path' => $path));

        if (Upload::is_valid())
        {
            Upload::save();

            // リネームとリサイズ
            $file = Upload::get_files(0);
            $to =  myUtil::getRandomString(32);
            $extension = $file['extension'];
            $to_file = $to.".".$extension;
            $from_file = $file['saved_as'];

            // オリジナルファイル
            copy($path.$from_file, $path.$to_file);
            if (file_exists($path.$to_file))
            {
                unlink($path.$from_file);
            
                // リサイズを実行
                $image = Image::load($path.$to_file);
                foreach ($sizes as $prifix => $size)
                {
                    $to_file_resize = $to."_".$prifix.".".$extension;
                    $image->crop_resize($size['w'],$size['h'])->save($path.$to_file_resize);
                }
                
                $image = Model_Image::forge();
                $image->name = $to;
                $image->path = $file_path;
                $image->extension = $extension;
                
                $filedata['image'] = $image;
            }
            else
            {
                $filedata['error'] = 'ファイルのアップロードに失敗しました。';
            }
        }
        else
        {
            $filedata['error'] = 'このファイルはアップロードできません。';
        }
        
        return $filedata;
    }


	/*
	 * データ登録時の処理
	 */
	public static function doDo($filedata)
	{
        $new_path = str_replace("/tmp/","/",$filedata['path']);
        $from_path = APPPATH . self::$base_path.$filedata['path'];
        $to_path = APPPATH . self::$base_path.$new_path;
        $error_flag = false;
        $ret = null;
        
        // ファイルをtmpディレクトリからコピー
        $sizes = array('',1,2,'t');
        foreach ($sizes as $size)
        {
            $filename = $filedata['name'];
            if ($size)
            {
                $filename .= "_".$size;
            }
            $filename .= ".".$filedata['extension'];

            copy($from_path.$filename, $to_path.$filename);
            if (file_exists($from_path.$filename) && file_exists($to_path.$filename))
            {
                unlink($from_path.$filename);
            }
            else
            {
                $error_flag = true;
            }
        }
        
        // エラーが発生しない場合、IDを登録する
        if (!$error_flag)
        {
            $image = Model_Image::forge();
            $image->path = $new_path;
            $image->name = $filedata['name'];
            $image->extension = $filedata['extension'];
            $image->save();
            
            $ret = $image->id;
        }
        
        return $ret;
    }


	/*
	 * データ削除
	 */
	public static function doDelete($image)
	{
        // サイズの設定
        $sizes = array("", 1,2,"t");
        $base_path = APPPATH . self::$base_path;
        
        foreach ($sizes as $size)
        {
            $file = $base_path.$image->path.$image->name;
            if ($size)
            {
                $file .= "_".$size;
            }
            $file .= ".".$image->extension;
            
            if (file_exists($file))
            {
                unlink($file);
            }
        }
    }
}
