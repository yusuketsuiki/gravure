<?php

class Model_Group extends \Orm\Model
{
    protected static $_properties = array(
		'id',
        'site_id',
        'name',
		'created_at',
		'updated_at',
	);

    protected static $_belongs_to = array(
        'site' => array(
            'key_from' => 'site_id',
            'model_to' => 'Model_Site',
            'key_to' => 'id',
            'cascade_save' => false, 
            'cascade_delete' => false, 
        ),
    );

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
    
    
    //-----------------------------------------
    // 枚数を更新
    //-----------------------------------------
    public function updatePictureNum()
    {
        $count = Model_Photo::find()->where("idol_id","=",$this->id)->where("delete_flag","=","1")->count();
        $this->picture_num = $count;
        $this->save();
        
    }
}
