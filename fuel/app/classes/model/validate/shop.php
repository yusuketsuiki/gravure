<?php

class Model_Validate_Shop extends \Orm\Model
{
    public static function validate($factory)
    {
        $val = Validation::forge($factory);

        $val->add_field('name', '店舗名', 'required|max_length[80]');
        $val->add_field('tdhk_id', '都道府県', 'required');
        $val->add_field('city_id', '市区町村', 'required');
        $val->add_field('company_id', '会社', 'required');
        $val->add_field('address', '住所', 'required|max_length[80]');
        $val->add_field('access', '最寄り駅', 'required');
        $val->add_field('tel', '電話番号', 'max_length[40]');
        $val->add_field('fax', 'FAX', 'max_length[20]');
        $val->add_field('open_time', '営業時間', 'required|max_length[80]');
        $val->add_field('holiday', '定休日', 'max_length[80]');
        $val->add_field('url', 'ホームページ', 'max_length[255]|valid_url');
        $val->add_field('blog', 'Blog', 'max_length[255]|valid_url');
        $val->add_field('twitter', 'Twitter', 'max_length[255]|valid_url');
        $val->add_field('facebook', 'Facebook', 'max_length[255]|valid_url');
        $val->add_field('capacity', '席数', 'max_length[20]');
        $val->add_field('establish', '設立日', 'max_length[20]');

        return $val;
    }
}