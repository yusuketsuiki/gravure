<?php

class Model_Validate_Friend extends \Orm\Model
{
    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_callable("myValidation");
        $val->add_field('body', '本文', 'required');
       return $val;
    }
}

