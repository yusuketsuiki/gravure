<?php

class Model_Validate_Mail extends \Orm\Model
{
    public static function validate($factory)
     {
         $val = Validation::forge($factory);
         $val->add_field('name', 'メール名', 'required|max_length[20]');
         $val->add_field('subject', '題名', 'required|max_length[80]');
         $val->add_field('body', '本文', 'required');
        return $val;
     }
}
