<?php

class Model_Validate_Admin extends \Orm\Model
{
    public static function validate($factory)
     {
         $val = Validation::forge($factory);
         $val->add_field('name', 'ログインID', 'required|min_length[4]');
         $val->add_field('password', 'パスワード', 'required|min_length[8]');
        return $val;
     }
}
