<?php

class Model_Validate_Time extends \Orm\Model
{
    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_field('money', '料金', 'required|max_length[10]|numeric_min[1]');
        return $val;
    }
}