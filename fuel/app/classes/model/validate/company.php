<?php

class Model_Validate_Company extends \Orm\Model
{
    public static function validate($factory)
    {
        $val = Validation::forge($factory);
        $val->add_field('name', '会社名', 'required|max_length[80]');
        $val->add_field('url', 'URL', 'max_length[80]|valid_url');
        $val->add_field('establish', '設立日', 'max_length[20]');
        return $val;
    }
}