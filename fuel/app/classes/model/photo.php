<?php

class Model_Photo extends \Orm\Model
{
    protected static $_properties = array(
		'id',
        'idol_id',
		'user_id',
        'photo_id',
		'secret',
        'farm',
        'server',
        'name',
        'is_tag',
        'is_text',
		'created_at',
		'updated_at',
	);

    protected static $_belongs_to = array(
        'user' => array(
            'key_from' => 'user_id',
            'model_to' => 'Model_User',
            'key_to' => 'id',
            'cascade_save' => false, 
            'cascade_delete' => false, 
        ),
        'idol' => array(
            'key_from' => 'idol_id',
            'model_to' => 'Model_Idol',
            'key_to' => 'id',
            'cascade_save' => false, 
            'cascade_delete' => false, 
        ),
    );


	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => true,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => true,
		),
	);
    
    
    //------------------------------------
    // 各画像のURL
    //------------------------------------
    // 各画像のベース用URL
    private function get_base()
    {
        return "http://farm".$this->farm.".staticflickr.com/".$this->server."/".$this->photo_id."_".$this->secret;
    }
    
    // スクエア取得
    public function get_q_150()
    {
        return $this->get_base()."_q.jpg";
    }

    // オリジナル取得
    public function get_o()
    {
        return $this->get_base()."_o.jpg";
    }

    // ラージ取得
    public function get_b()
    {
        return $this->get_base()."_b.jpg";
    }

    // ミディアム
    public function get_m_640()
    {
        return $this->get_base()."_z.jpg?zz=1";
    }
    public function get_m_800()
    {
        return $this->get_base()."_c.jpg";
    }


    
    // 元ページ
    public function get_page()
    {
        return "http://www.flickr.com/photos/".$this->user->owner."/".$this->photo_id;
    }
}
