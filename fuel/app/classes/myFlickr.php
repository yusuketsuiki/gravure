<?php
class myFlickr
{
    public static $key = "e85ab7810d0483d3b253246268e04e36";
    public static $secret = "76874d88525c4140";
    public static $per_page = 100;


    /*
    * Name : photosSearch
    * Func : 画像検索
    */
    public static function photosSearch($idol)
    {
        try
        {
            // タグモード、テキストモードで検索
            $modes = array("tags","text");
        
            foreach ($modes as $mode)
            {
                // 初期設定
                $page = 1;
                $total_page = 0;
                $break_flag = false;
                
                while(1)
                {
                    $url = 'http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key='.self::$key.'&'.$mode.'='.urlencode($idol->name).'&sort=date-posted-desc&format=rest&page='.$page.'&per_page='.self::$per_page;
                    $rest = file_get_contents($url);
                    $xml = new SimpleXMLElement($rest);
    
                    // 1ページ目の場合、totalページを取得
                    if ($page == 1)
                    {
                        $total_page = $xml->photos["pages"];
                        if ($total_page == 0)
                        {
                            echo "total_page is none:$mode\n";
                            break;
                        }
                    }
                    
                    foreach ($xml->photos->photo as $photo_data)
                    {
                        echo $mode."\n";
                        // 画像が登録されているか？
                        $photo = Model_Photo::find()->where("photo_id","=",$photo_data["id"])->get_one();
                        
                        // 新規登録
                        if (!($photo instanceof Model_Photo))
                        {
                            // ユーザ登録済？
                            $user = Model_User::find()->where("owner","=",$photo_data["owner"])->get_one();
                            if (!($user instanceof Model_User))
                            {
                                $user_data = self::peopleGetInfo($photo_data["owner"]);

                                // 新規登録
                                $user = new Model_User();
                                $user->owner = $photo_data["owner"];
                                $user->name = $user_data->person->username;
                                $user->photosurl = $user_data->person->photosurl;
                                $user->profileurl = $user_data->person->profileurl;
                                $user->mobileurl = $user_data->person->mobileurl;
                                $user->save();
                            }
                            
                            // 画像登録
                            $photo = new Model_Photo();
                            $photo->idol_id = $idol->id;
                            $photo->user_id = $user->id;
                            $photo->photo_id = $photo_data["id"];
                            $photo->secret = $photo_data["secret"];
                            $photo->farm = $photo_data["farm"];
                            $photo->server = $photo_data["server"];
                            $photo->name = $photo_data["title"];
                            if ($mode == "text")
                            {
                                $photo->is_text = "1";
                                $photo->is_tag = "X";
                            }
                            elseif ($mode == "tags")
                            {
                                $photo->is_text = "X";
                                $photo->is_tag = "1";
                            }
                            $photo->save();
                        }
                        else
                        {
                            // モードに応じた読み込みが完了した場合、ブレイク
                            if ($mode == "text")
                            {
                                if ($photo->is_text == "1")
                                {
                                    echo "is_text_break\n";
                                    $break_flag = true;
                                    break;
                                }
                                $photo->is_text = 1;
                            }
                            elseif ($mode == "tags")
                            {
                                if ($photo->is_tag == "1")
                                {
                                    echo "is_tag_break\n";
                                    $break_flag = true;
                                    break;
                                }
                                $photo->is_tag = 1;
                            }
                            $photo->save();
                        }
                    }

                    // モードに応じて最後まで読んだ
                    if ($break_flag)
                    {
                        break;
                    }

                    // 最後まで読んだ
                    $page++;
                    if ($page > $total_page)
                    {
                        echo "page > total_page\n";
                        break;
                    }
                }
            }
            
            // 最後にアイドルの枚数を更新
            $idol->updatePictureNum();
        }
        catch (exception $e)
        {
            echo $e;
            exit;
        }
    }


    /*
    * Name : peopleGetInfo
    * Func : ユーザ検索
    */
    public static function peopleGetInfo($owner)
    {
        try
        {
            $url = 'http://api.flickr.com/services/rest/?method=flickr.people.getInfo&api_key='.self::$key.'&user_id='.urlencode($owner).'&format=rest';
            $rest = file_get_contents($url);
            $xml = new SimpleXMLElement($rest);
            
            return $xml;
        }
        catch (exception $e)
        {
            echo $e;
            exit;
        }
    }

}