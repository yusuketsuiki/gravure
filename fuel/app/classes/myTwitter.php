<?php
class myTwitter
{
	public static $consumer_key = 'Ri0icLbwo0feLJDk9cpig';
	public static $consumer_secret = 'insY2ItW1akXC7cpONjfqpaGbOII2C8jbPhR8dmQ';

    /*
    * Name : getAuthUrl
    * Func : 認証用のURLを取得
    */
    public static function getAuthUrl($callback)
    {
        try
        {
            // トークンを取得
            $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret);
            $http_request = new HTTP_Request2();
            $http_request->setConfig('ssl_verify_peer', false);
            $consumer_request = new HTTP_OAuth_Consumer_Request;
            $consumer_request->accept($http_request);
            $consumer->accept($consumer_request);

            $consumer->getRequestToken('https://api.twitter.com/oauth/request_token', $callback);
            
            // トークンをCookieに保存
            Cookie::set("twitter_token",$consumer->getToken());
            Cookie::set("twitter_token_secret",$consumer->getTokenSecret());
            
            // 認証用のURLを取得
            $auth_url = $consumer->getAuthorizeUrl('http://twitter.com/oauth/authorize');
    
            return $auth_url;
        }
        catch (exception $e)
        {
            echo $e;
            exit;
        }
    }


    /*
    * Name : doAuth
    * Func : Twitter認証
    */
    public static function doAuth()
    {
        try
        {
            $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret);
            $http_request = new HTTP_Request2();
            $http_request->setConfig('ssl_verify_peer', false);
            $consumer_request = new HTTP_OAuth_Consumer_Request;
            $consumer_request->accept($http_request);
            $consumer->accept($consumer_request);
            
            $verifier = $_GET['oauth_verifier'];
            $request_token = $_GET['oauth_token'];

            $consumer->setToken(Cookie::get("twitter_token"));
            $consumer->setTokenSecret(Cookie::get("twitter_token_secret"));
            $consumer->getAccessToken('https://api.twitter.com/oauth/access_token', $verifier);

            // ユーザ情報を取得
            $result = $consumer->sendRequest('https://api.twitter.com/1.1/statuses/user_timeline.json', array(), 'GET');
            $content = json_decode(mb_convert_encoding($result->getBody(), 'EUC-JP', 'UTF-8'));
        }
        catch (exception $e)
        {
            echo "kita";
            var_dump($e->getMessage());
            return null;
        }
        
        if (!empty($content->errors))
        {
            var_dump($content->errors);
            return null;
        }
        
        // 画像URL
        $base_image_url = $content[0]->user->profile_image_url;
        $image_list = array(
            'original' => str_replace("_normal","",$base_image_url), // 原寸大
            'reasonably_small' => str_replace("_normal","_reasonably_small",$base_image_url), // 73x73
            'normal' => $base_image_url, // 48x48
            'mini' => str_replace("_normal","_mini",$base_image_url), // 24x24
        );
        
        // リターン
        $twitter_data = array('');
        $twitter_data['user_id'] = $content[0]->user->id;
        $twitter_data['name'] = $content[0]->user->name;
        $twitter_data['description'] = $content[0]->user->description;
        $twitter_data['screen_name'] = $content[0]->user->screen_name;
        $twitter_data['image_list']   = $image_list;
        $twitter_data['token'] = $consumer->getToken();
        $twitter_data['token_secret'] = $consumer->getTokenSecret();
        
        return $twitter_data;
    }


    /*
    * Name : doUpdate
    * Func : Twitterへ投稿
    */
    public static function doUpdate($token, $token_secret, $data)
    {
        try
        {
            $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
            $http_request = new HTTP_Request2();
            $http_request->setConfig('ssl_verify_peer', false);
            $consumer_request = new HTTP_OAuth_Consumer_Request;
            $consumer_request->accept($http_request);
            $consumer->accept($consumer_request);

            $response = $consumer->sendRequest('https://api.twitter.com/1.1/statuses/update.json', $data, 'POST');
            return json_decode(mb_convert_encoding($response->getBody(), 'EUC-JP', 'UTF-8'));
        }
        catch (exception $e)
        {
            return null;
        }
    }


    /*
    * Name : doNewDm
    * Func : ダイレクトメッセージ
    */
    public static function doNewDm($token, $token_secret, $data)
    {
        try
        {
            $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
            $http_request = new HTTP_Request2();
            $http_request->setConfig('ssl_verify_peer', false);
            $consumer_request = new HTTP_OAuth_Consumer_Request;
            $consumer_request->accept($http_request);
            $consumer->accept($consumer_request);


            $response = $consumer->sendRequest('https://api.twitter.com/1.1/direct_messages/new.json', $data, 'POST');
        }
        catch(exception $e)
        {
            return null;
        }
        
        return json_decode(mb_convert_encoding($response->getBody(), 'EUC-JP', 'UTF-8'));
    }



    /*
    * Name : getFriends
    * Func : friendを取得
    */
    public static function getFriends($token, $token_secret, $data)
    {
        $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
        $http_request = new HTTP_Request2();
        $http_request->setConfig('ssl_verify_peer', false);
        $consumer_request = new HTTP_OAuth_Consumer_Request;
        $consumer_request->accept($http_request);
        $consumer->accept($consumer_request);

        $ret = array();
        $cursor = -1;
        $i = 0;

        while (1)
        {
            $result_data = $consumer->sendRequest('https://api.twitter.com/1.1/friends/ids.json?cursor='.$cursor, $data, 'GET');
            $result = json_decode(mb_convert_encoding($result_data->getBody(), 'EUC-JP', 'UTF-8'));
            
            $ret = array_merge($ret, $result->ids);
            
            if ($result->next_cursor)
            {
                $cursor = $result->next_cursor;
            }
            else
            {
                break;
            }

            if ($i > 5)
            {
                break;
            }
            
            $i++;
        }
        
        return $ret;
    }


    /*
    * Name : getFollowers
    * Func : followerを取得
    */
    public static function getFollowers($token, $token_secret, $data)
    {
        $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
        $http_request = new HTTP_Request2();
        $http_request->setConfig('ssl_verify_peer', false);
        $consumer_request = new HTTP_OAuth_Consumer_Request;
        $consumer_request->accept($http_request);
        $consumer->accept($consumer_request);

        $ret = array();
        $cursor = -1;
        $i = 0;

        while (1)
        {
            $result_data = $consumer->sendRequest('https://api.twitter.com/1.1/followers/ids.json?cursor='.$cursor, $data, 'GET');
            $result = json_decode(mb_convert_encoding($result_data->getBody(), 'EUC-JP', 'UTF-8'));
            
            $ret = array_merge($ret, $result->ids);
            
            if ($result->next_cursor)
            {
                $cursor = $result->next_cursor;
            }
            else
            {
                break;
            }

            if ($i > 5)
            {
                break;
            }
            
            $i++;
        }

        return $ret;
    }


    /*
    * Name : userLookup
    * Func : friendを取得
    */
    public static function userLookup($token, $token_secret, $data)
    {
        $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
        $http_request = new HTTP_Request2();
        $http_request->setConfig('ssl_verify_peer', false);
        $consumer_request = new HTTP_OAuth_Consumer_Request;
        $consumer_request->accept($http_request);
        $consumer->accept($consumer_request);

        //$user_id_hash = myUtil::ArrayToHash(explode(",",$data['user_id']));

        $json_result = $consumer->sendRequest('https://api.twitter.com/1.1/users/lookup.json', $data, 'GET');
        $result = json_decode(mb_convert_encoding($json_result->getBody(), 'EUC-JP', 'UTF-8'));

        /*
        $get_id_hash = array();
        foreach ($result as $d)
        {
            echo "d:".$d->id_str."\n";
            $get_id_hash[$d->id_str] = true;
        }
        
        foreach ($user_id_hash as $user_id => $trush)
        {
            echo "user_id:".$user_id."\n";
            if (empty($get_id_hash[$user_id]))
            {
                echo "nai:".$user_id."\n";
            }
        }
        
        var_dump(count($data));
        exit;
        */
        
        return $result;

    }


    /*
    * Name : getUserData
    * Func : ユーザデータを取得
    */
    public static function getUserData($token, $token_secret)
    {
        $consumer = new HTTP_OAuth_Consumer(self::$consumer_key, self::$consumer_secret,$token,$token_secret);
        $http_request = new HTTP_Request2();
        $http_request->setConfig('ssl_verify_peer', false);
        $consumer_request = new HTTP_OAuth_Consumer_Request;
        $consumer_request->accept($http_request);
        $consumer->accept($consumer_request);
        $result = $consumer->sendRequest('https://api.twitter.com/1.1/statuses/user_timeline.json', array(), 'GET');
        $content = json_decode(mb_convert_encoding($result->getBody(), 'EUC-JP', 'UTF-8'));

        return $content;

    }

}