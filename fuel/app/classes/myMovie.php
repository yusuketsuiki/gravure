<?php
class myMovie
{
    //--------------------------------------------
    // URLから評価を計算
    //--------------------------------------------
    public static function getPoint($movie_no)
    {
        //----------------------------------------
        // 映画情報を取得
        //----------------------------------------
        // まず映画NOを抜き出す
        $url = 'http://info.movies.yahoo.co.jp/detail/tymv/'.$movie_no.'/';
        $movie_no = "";
        
        if (preg_match("/tymv\/(.+)\//",$url,$match))
        {
            $movie_no = $match[1];
        }
        
        // DBに登録済か
        $movie = \Model_Movie::find()->where('movie_no','=',$movie_no)->get_one();
        if (!($movie instanceof \Model_Movie))
        {
            // 映画名を取得
            $html = file_get_contents($url);
            $html = mb_convert_encoding($html, "utf-8", "euc-jp");

            $name = "";
            if (preg_match("/<title>(.+?) - Yahoo\!映画/",$html,$match))
            {
                $name = $match[1];
            }
        
            $movie = \Model_Movie::forge();
            $movie->name = $name;
            $movie->movie_no = $movie_no;
            $movie->save();
        }
        
        echo "【映画】".$movie->name."\n";

        //----------------------------------------
        // レビューを検索
        //----------------------------------------
        // レビュー用のURLを作成
        $base_url = "http://info.movies.yahoo.co.jp/userreview/tymv/".$movie_no."/s0/p%page%";
        $page = 1;
        
        $user_list = array();
        while(1)
        {
            echo $page."ページを読み込み...\n";
        
            // URLを作成
            $review_url = str_replace("%page%",$page, $base_url);
            $review_html = file_get_contents($review_url);
            
            $review_html = mb_convert_encoding($review_html, "utf-8", "euc-jp");
            $review_html = str_replace("\n","",$review_html);
            $review_html = str_replace("\r","",$review_html);
            $review_html = str_replace("\r\n","",$review_html);
            
            // 総合件数を取得する
            $total_num = 0;
            if (preg_match('/<span class=\"b\">(\d+?)<\/span>件中/',$review_html,$match))
            {
                $total_num = $match[1];
            }
            
            //----------------------------------------
            // ユーザの採点を取得
            //----------------------------------------
            // 点数一覧を取得
            $point_list =  "";
            if (preg_match_all('/star_m_ylw\.gif\" alt=（5点満点中(\d?)点/', $review_html, $match))
            {
                $point_list = $match[1];
            }
            
            // ユーザ一覧を取得
            $tmp_user_list = "";
            if (preg_match_all("/投稿日時：\d+?\/\d+?\/\d+? \d+?:\d+?  投稿者：(.+?)さん<\/p>/",$review_html,$match))
            {
                $tmp_user_list = $match[1];
            }
            
            // ユーザIDを取得
            $last_prof_no = "";
            
            foreach ($tmp_user_list as $i => $user)
            {
                // ユーザ情報取得
                if (preg_match("/my\.movies\.yahoo\.co\.jp\/profile-(.+?)\">(.+?)</",$user, $match))
                {
                    $user_list[$match[1]] = array(
                        'prof_no' => $match[1],
                        'name' => $match[2],
                        'prof_flag' => '0',
                        'point' => $point_list[$i],
                    );
                    $last_prof_no = $match[1];
                }
                else
                {
                    $user_list[$user] = array(
                        'prof_no' => $user,
                        'name' => $user,
                        'prof_flag' => 'X',
                        'point' => $point_list[$i],
                    );
                    $last_prof_no = $user;
                }
            }
            
            //----------------------------------
            // 次のページを読むか判定
            // (1)最後のページまで読んだ
            // (2)該当ページ内の最後のユーザが評価登録済
            // のどちらかの場合は、次ページへ行かない
            //----------------------------------
            $next_flag = true;

            // (1)最後のページまで読んだか
            if(ceil($total_num / 10) == $page)
            {
                $next_flag = false;
            }
            
            // 該当ページ内の最後のユーザが評価登録済か
            if ($next_flag)
            {
                // ユーザ登録済？
                $user = \Model_User::find()->where('prof_no','=',$last_prof_no)->get_one();
                
                if ($user instanceof \Model_User)
                {
                    $movieuser = \Model_Movieuser::find()->where('movie_id','=',$movie->id)->where('user_id','=',$user->id)->get_one();
                    if ($movieuser instanceof \Model_Movieuser)
                    {
                        $next_flag= false;
                    }
                }
            }
            
            if (!$next_flag)
            {
                break;
            }

            // 読み込みページをインクリメント
            $page++;
        }
        
        //--------------------------------------
        // 未登録ユーザを登録する
        //--------------------------------------
        // まずDBに登録されているかを確認
        // in検索用のプロフィールナンバーリストを作る
        $prof_no_in = array();
        foreach ($user_list as $prof_no => $user)
        {
            $prof_no_in[] = $prof_no;
        }
        
        $already_user_hash = array();
        $tmp_already_user_list = \Model_User::find()->where('prof_no','in',$prof_no_in)->get();
        
        // ハッシュ化
        foreach ($tmp_already_user_list as $tmp_already_user)
        {
            $already_user_hash[$tmp_already_user->prof_no] = $tmp_already_user;
        }
        
        // 登録
        foreach ($user_list as $prof_no => $u)
        {
            // 未登録ユーザか
            if (empty($already_user_hash[$prof_no]))
            {
                sleep(2);
                echo "登録：".$u['name']."\n";
            
                $user = \Model_User::forge();
                $user->name = $u['name'];
                $user->prof_no = $u['prof_no'];
                $user->prof_flag = $u['prof_flag'];
                $user->getted_at = date('Y-m-d H:i:s');
                
                // review_numを登録
                if ($u['prof_flag'] == '0')
                {
                    $user->getReviewNum();
                }
                
                $user->save();
                
                $user_list[$prof_no]['id'] = $user->id;
            }
            else
            {
                $user_list[$prof_no]['id'] = $already_user_hash[$prof_no]->id;
            }
        }

        //--------------------------------------
        // ポイントを登録する
        //--------------------------------------
        // 登録済のポイントを取得
        $already_movieuser_list = \Model_Movieuser::find()->related('user')->where('movie_id','=',$movie->id)->where('user.prof_no','in',$prof_no_in)->get();
        
        // ハッシュ化
        $already_movieuser_hash = array();
        foreach ($already_movieuser_list as $movieuser)
        {
            $already_movieuser_hash[$movieuser->user->prof_no] = true;
        }
        
        // 登録
        foreach($user_list as $prof_no => $u)
        {
            if (empty($already_movieuser_hash[$prof_no]))
            {
                $movieuser = \Model_Movieuser::forge();
                $movieuser->movie_id = $movie->id;
                $movieuser->user_id = $u['id'];
                $movieuser->point = $u['point'];
                $movieuser->save();
            }
        }
        
        $movie->doCalculation();
        
    }
}
