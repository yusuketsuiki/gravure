<?php
class Pager
{
	protected static $peer;
	protected $total_count;
	protected $total_page = '';
	protected $page = '';
	protected $start = '';
	protected $end = '';
	protected $start_page = '';
	protected $end_page = '';
	protected $offset = '';
	protected $rule = '';
	protected $option = null;

	public function getTotalCount()
	{
		return $this->total_count;
	}

	public function getTotalPage()
	{
		return $this->total_page;
	}

	public function getPage()
	{
		return $this->page;
	}

	public function getStart()
	{
		return $this->start;
	}

	public function getEnd()
	{
		return $this->end;
	}

	public function getStartPage()
	{
		return $this->start_page;
	}

	public function getEndPage()
	{
		return $this->end_page;
	}

	public function getRule()
	{
		return $this->rule;
	}

	public function getOption()
	{
		return $this->option;
	}

	public function getOffset()
	{
		return $this->offset;
	}

	public function getOptionStr()
	{
		$ret = "";
		if (is_array($this->option))
		{
			foreach ($this->option as $key => $value)
			{
				$ret .= "&".$key."=".$value;
			}
		}
		return $ret;
	}



	public function setTotalCount($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->total_count !== $v) {
			$this->total_count = $v;
		}
	} 

	public function setTotalPage($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->total_page !== $v) {
			$this->total_page = $v;
		}
	} 

	public function setPage($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->page !== $v) {
			$this->page = $v;
		}
	} 

	public function setStart($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->start !== $v) {
			$this->start = $v;
		}
	} 

	public function setEnd($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->end !== $v) {
			$this->end = $v;
		}
	} 

	public function setStartPage($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->start_page !== $v) {
			$this->start_page = $v;
		}
	} 

	public function setEndPage($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->end_page !== $v) {
			$this->end_page = $v;
		}
	} 
 
	public function setRule($v)
	{
		if ($v !== null && !is_string($v)) {
			$v = (string) $v; 
		}

		if ($this->rule !== $v || $v === '') {
			$this->rule = $v;
		}
	}

	public function setOption($v)
	{
		if ($v !== null && !is_array($v))
		{
			$v = (array) $v; 
		}

		if ($this->option !== $v || $v === null)
		{
			$this->option = $v;
		}
	}


	public function setOffset($v)
	{
		if ($v !== null && !is_int($v) && is_numeric($v)) {
			$v = (int) $v;
		}

		if ($this->offset !== $v) {
			$this->offset = $v;
		}
	} 

	public function getPageList()
	{
		$page_list = array();
		for ($i = 10; $i > 0; $i--)
		{
			if (($this->page - $i) > 0)
			{
				$page_list[] = ($this->page - $i);
			}
		}

		$page_list[] = $this->page;

		for ($i = 1; $i <= 10 ; $i++)
		{
			if (($this->page + $i) <= $this->total_page)
			{
				$page_list[] = ($this->page + $i);
			}
		}
		return $page_list;
	}

	public function getOptions()
	{
		$ret = "";

        if (count($this->getOption()) > 0)
        {
            foreach ($this->getOption() as $key => $value)
            {
                $ret .= '&'.$key.'='.$value;
            }
        }
        return $ret;
	}

 /**
	* Name : setDefault
	* Func : 
	*/
	public function __construct($total_count,$rule,$count = 10, $option = null,$disp = 10)
	{
        // ページ数判定
        $page = 1;
        $get = Input::get();
        if (!empty($get['p']))
        {
            $page = $get['p'];
        }
      
        // 合計ページ取得
		$total_page	= ceil($total_count / $count);

		// Start,End
		$start = ($page - 1) * $count + 1;
		$end	 = $page * $count;
		if ($end > $total_count)
		{
			$end = $total_count;
		}
	
		// StartPage、EndPage
		$start_page = 1;
		$end_page = $total_page;
		if ($disp)
		{
			if (($page - $disp) < 1)
			{
				$start_page = 1;
				$end_page = 1 + ($disp * 2);
				if ($end_page > $total_page)
				{
					$end_page = $total_page;
				}
			}
			elseif (($page + $disp) > $total_page)
			{
				$end_page = $total_page;
				$start_page = $end_page - ($disp * 2);
				if ($start_page < 1)
				{
					$start_page = 1;
				}
			}
			else
			{
				$start_page = $page - $disp;
				$end_page = $page + $disp;
			}
		}

		// ページャに値を設定
		$this->setTotalCount($total_count);
		$this->setTotalPage($total_page);
		$this->setPage($page);
		$this->setOption($option);
		$this->setRule($rule);
		$this->setStart($start);
		$this->setEnd($end);
		$this->setOffset($start - 1);
		$this->setStartPage($start_page);
		$this->setEndPage($end_page);
	}
} 