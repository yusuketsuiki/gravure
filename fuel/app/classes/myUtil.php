<?php
class myUtil
{
    /*
    * Name : ArrayToHash
    * Func : 配列をハッシュ化
    */
    public static function ArrayToHash($array)
    {
        $ret = array();
        foreach ($array as $value)
        {
            $ret[$value] = true;
        }
        
        return $ret;
    }


    /*
    * Name : MailSender
    * Func : メール送信
    */
    public static function MailSender($mfrom, $mto, $mttl, $mmess, $mcc = null, $mbcc = null, $fileup_fld = null, $upfile = null, $upfl_type = null, $upfl_name = null)
    {
        echo "kita4";
        // ローカルの場合はメールを送らない
        if ((empty($_SERVER['SERVER_ADDR'])) || ($_SERVER['SERVER_ADDR'] == '127.0.0.1'))
        {
            return;
        }
    
        echo "kita5";
        // 件名の文字エンコード
        // "auto"で文字化けが発生する場合は、"ASCII,JIS,UTF-8,EUC-JP,SJIS"と指定
        // それでも文字化けが発生する場合は、ini_set()を使用してPHPや入力、出力の文字コードを指定
        $xSubject = mb_convert_encoding($mttl, 'JIS', 'ASCII,JIS,UTF-8,EUC-JP,SJIS');
        $xSubject = base64_encode($xSubject);
        $xSubject = '=?iso-2022-jp?B?'.$xSubject.'?=';

        // メール本文の文字エンコード
        $Message = mb_convert_encoding($mmess, 'JIS', 'ASCII,JIS,UTF-8,EUC-JP,SJIS');
        
        // メールのヘッダ作成
        $GMT = date('Z');
        $GMT_ABS  = abs($GMT);
        $GMT_HOUR = floor($GMT_ABS / 3600);
        $GMT_MIN = floor(($GMT_ABS - $GMT_HOUR * 3600) / 60);
        $GMT_FLG = $GMT>=0 ? '+' : '-';
        $GMT_RFC = date('D, d M Y H:i:s ').sprintf($GMT_FLG.'%02d%02d', $GMT_HOUR, $GMT_MIN);
        
        $Headers  = "Date: ".$GMT_RFC."\n";
        $Headers .= "From: $mfrom\n";
        if($mcc) $Headers .= "Cc: $mcc\n";
        if($mbcc) $Headers .= "Bcc: $mbcc\n";
        $Headers .= "Subject: $xSubject\n";
        $Headers .= "MIME-Version: 1.0\n";
        $Headers .= "X-Mailer: PHP/".phpversion()."\n";
    
        // 添付ファイルがある場合の処理(1個のみ)
        if($upfile!='' && @file_exists($fileup_fld.$upfile))
        {
            $boundary = md5(uniqid(rand(), 1));
            
            // メールのヘッダへ追記処理
            $Headers .= "Content-Type: multipart/mixed;\n";
            $Headers .= "\tboundary=\"".$boundary."\"\n";
            
            // メール本文に追記処理(マルチパート処理)
            $xMessage = "This is a multi-part message in MIME format.\n\n";
            $xMessage .= "--".$boundary."\n";
            $xMessage .= $Message;
            $fp = @fopen($fileup_fld.$upfile, 'r');
            $contents = @fread($fp, filesize($fileup_fld.$upfile));
            fclose($fp);
            $fname = mb_convert_encoding($upfl_name, 'JIS', 'ASCII,JIS,UTF-8,EUC-JP,SJIS');
            $upfile = mb_convert_encoding($upfile, 'JIS', 'ASCII,JIS,UTF-8,EUC-JP,SJIS');
            $f_encoded = chunk_split(base64_encode($contents));
            $xMessage .= "\n\n--$boundary\n";
            $xMessage .= "Content-Type: ".$upfl_type.";\n";
            $xMessage .= "\tname=\"".$fname."\"\n";
            $xMessage .= "Content-Transfer-Encoding: base64\n";
            $xMessage .= "Content-Disposition: attachment;\n";
            $xMessage .= "\tfilename=\"".$fname."\"\n\n";
            $xMessage .= $f_encoded."\n";
            $xMessage .= "--".$boundary."--";
        }
        // 添付ファイルがある場合の処理(複数個)
        elseif($upfile && is_array($upfile))
        {
            $boundary = md5(uniqid(rand(), 1));
    
            // メールのヘッダへ追記処理
            $Headers .= "Content-Type: multipart/mixed;\n";
            $Headers .= "\tboundary=\"".$boundary."\"\n";
            
            // メール本文に追記処理(マルチパート処理)
            $xMessage = "This is a multi-part message in MIME format.\n\n";
            $xMessage .= "--".$boundary."\n";
            $xMessage .= $Message;
            $imax = count($upfile);

            for($i=0; $i<$imax; $i++)
            {
                $file = $fileup_fld.$upfile[$i];
                if(file_exists($file))
                {
                    $fp = @fopen($file, 'r');
                    $contents = @fread($fp, filesize($file));
                    fclose($fp);
                    $fname = mb_convert_encoding($upfl_name[$i], 'JIS', 'ASCII,JIS,UTF-8,EUC-JP,SJIS');
                    $f_encoded = chunk_split(base64_encode($contents));
                    $xMessage .= "\n\n--$boundary\n";
                    $xMessage .= "Content-Type: ".$upfl_type[$i].";\n";
                    $xMessage .= "\tname=\"".$fname."\"\n";
                    $xMessage .= "Content-Transfer-Encoding: base64\n";
                    $xMessage .= "Content-Disposition: attachment;\n";
                    $xMessage .= "\tfilename=\"".$fname."\"\n\n";
                    $xMessage .= $f_encoded."\n";
                }
            }
            $xMessage .= "--".$boundary."--";
        }
        // 添付ファイルがない場合の処理
        else
        { 
            $Headers .= "Content-type: text/plain; charset=ISO-2022-JP\n";
            $Headers .= "Content-Transfer-Encoding: 7bit";
            $xMessage = $Message;
        }

        // メール送信
        // $mto：メールの送信先
        // $xSubject：文字エンコード済みのメールの件名
        // $xMessage：文字エンコード済みのメール本文
        // $Headers：メールのヘッダ
        echo "kita";
        return mail($mto, $xSubject, $xMessage, $Headers);
    }


   
     /*
	 * ランダム文字列を取得
	 */
	public static function getRandomString($nLengthRequired = 8)
	{
		$sCharList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
		mt_srand();
		$sRes = "";
		for($i = 0; $i < $nLengthRequired; $i++)
		$sRes .= $sCharList{mt_rand(0, strlen($sCharList) - 1)};
		return $sRes;
	}


	/*
	 * Name : getTdhkList
	 * Func : 都道府県リストを取得
	*/
	public static function getTdhkList($str = '')
	{
		$ret = array();

		if ($str)
		{
			$ret[''] = $str;
		}
		$ret['北海道'] = '北海道';
		$ret['青森県'] = '青森県';
		$ret['岩手県'] = '岩手県';
		$ret['宮城県'] = '宮城県';
		$ret['秋田県'] = '秋田県';
		$ret['山形県'] = '山形県';
		$ret['福島県'] = '福島県';
		$ret['茨城県'] = '茨城県';
		$ret['栃木県'] = '栃木県';
		$ret['群馬県'] = '群馬県';
		$ret['埼玉県'] = '埼玉県';
		$ret['千葉県'] = '千葉県';
		$ret['東京都'] = '東京都';
		$ret['神奈川県'] = '神奈川県';
		$ret['山梨県'] = '山梨県';
		$ret['新潟県'] = '新潟県';
		$ret['富山県'] = '富山県';
		$ret['石川県'] = '石川県';
		$ret['福井県'] = '福井県';
		$ret['長野県'] = '長野県';
		$ret['岐阜県'] = '岐阜県';
		$ret['静岡県'] = '静岡県';
		$ret['愛知県'] = '愛知県';
		$ret['三重県'] = '三重県';
		$ret['滋賀県'] = '滋賀県';
		$ret['京都府'] = '京都府';
		$ret['大阪府'] = '大阪府';
		$ret['兵庫県'] = '兵庫県';
		$ret['奈良県'] = '奈良県';
		$ret['和歌山県'] = '和歌山県';
		$ret['鳥取県'] = '鳥取県';
		$ret['島根県'] = '島根県';
		$ret['岡山県'] = '岡山県';
		$ret['広島県'] = '広島県';
		$ret['山口県'] = '山口県';
		$ret['徳島県'] = '徳島県';
		$ret['香川県'] = '香川県';
		$ret['愛媛県'] = '愛媛県';
		$ret['高知県'] = '高知県';
		$ret['福岡県'] = '福岡県';
		$ret['佐賀県'] = '佐賀県';
		$ret['長崎県'] = '長崎県';
		$ret['熊本県'] = '熊本県';
		$ret['大分県'] = '大分県';
		$ret['宮崎県'] = '宮崎県';
		$ret['鹿児島県'] = '鹿児島県';
		$ret['沖縄県'] = '沖縄県';

		return $ret;
	}


  /*
   * Name : getStrlenStr
   * Func : 文字を切り詰める
  */
  public static function getStrlenStr($str, $num = 10, $sep = '…')
  {
    if (mb_strlen($str,'UTF-8') > $num)
    {
      $str = mb_substr($str,0,$num,'UTF-8').$sep;
    }
    return $str;
  }


 /**
  * Name : getYmdList
  * Func : 年月日リストを取得する
  */
  public static function getYmdList($mode,$year_start,$year_end, $year_def = '',$month_def = '',$day_def = '', $hour_def = '', $min_def = '', $sec_def = '')
  {
    $year_list = array();
    $month_list = array();
    $day_list = array();
    $hour_list = array();
    $min_list = array();
    $sec_list = array();
    
    if ($year_def != '')
    {
      $year_list = array('' => $year_def);
    }

    if ($month_def != '')
    {
      $month_list = array('' => $month_def);
    }

    if ($day_def != '')
    {
      $day_list = array('' => $day_def);
    }

    /*
    $hour_list = array('' => $hour_def);
    $min_list = array('' => $min_def);
    $sec_list = array('' => $sec_def);
    */
    
    if ($mode == "reverse")
    {
        for($i = $year_start; $i >= $year_end; $i--)
        {
          $year_list[$i] = "$i";
        }
    }
    else
    {
        for($i = $year_start; $i <= $year_end; $i++)
        {
          if ($mode == 'nengou')
          {
            $nengou = "";
            
            if ($i <= 1925)
            {
              $j = $i - 1911;
              $nengou  = "大正".$j."年";
            }
            elseif ($i <= 1988)
            {
              $j = $i - 1925;
              $nengou  = "昭和".$j."年";
            }
            else
            {
              $j = $i - 1988;
              $nengou  = "平成".$j."年";
            }
            
            $year_list[$i] = "$i($nengou)";
          }
          else
          {
            $year_list[$i] = "$i";
          }
        }
    }

    for($i = 1; $i <= 12; $i++)
    {
      if (strlen($i) == 1)
      {
        $month_list['0'.$i] = '0'.$i;
      }
      else
      {
        $month_list[$i] = $i;
      }
    }

    for($i = 1; $i <= 31; $i++)
    {
      if (strlen($i) == 1)
      {
        $day_list['0'.$i] = '0'.$i;
      }
      else
      {
        $day_list[$i] = $i;
      }
    }

    for($i = 0; $i <= 23; $i++)
    {
      if (strlen($i) == 1)
      {
        $hour_list['0'.$i] = '0'.$i;
      }
      else
      {
        $hour_list[$i] = $i;
      }
    }

    //for($i = 0; $i <= 59; $i = $i+=5)
    for($i = 0; $i <= 59; $i++)
    {
      if (strlen($i) == 1)
      {
        $min_list['0'.$i] = '0'.$i;
      }
      else
      {
        $min_list[$i] = $i;
      }
    }

    for($i = 0; $i <= 59; $i++)
    {
      if (strlen($i) == 1)
      {
        $sec_list['0'.$i] = '0'.$i;
      }
      else
      {
        $sec_list[$i] = $i;
      }
    }
    
    $ymd_list = array('year_list' => $year_list, 'month_list' => $month_list, 'day_list' => $day_list, 'hour_list' => $hour_list, 'min_list' => $min_list, 'sec_list' => $sec_list);
    return $ymd_list;
  }


    /*
    * Name : convertDateJp
    * Func : YYYYMMDDをYYYY年MM月DD日に変換
    */
    public static function convertDateJp($str)
    {
        $ret = $str;
        if (strlen($str) == 8)
        {
            $ret = substr($str,0,4)."年".substr($str,4,2)."月".substr($str,6,2)."日";
        }
        return $ret;
    }
    

    //--------------------------------------------
    // 日にちの二桁変換(1→01)
    //--------------------------------------------
    public static function addZeroDay($day)
    {
        if (strlen($day) == 1)
        {
            $day = "0".$day;
        }
        return $day;
    }


    //--------------------------------------------
    // 曜日
    //--------------------------------------------
    public static function getYoubiStr($date,$lang = "ja")
    {
        $ja_list = array('0' => '日','1' => '月','2' => '火','3' => '水','4' => '木','5' => '金','6' => '土');
        $ja_full_list = array('0' => '日曜日','1' => '月曜日','2' => '火曜日','3' => '水曜日','4' => '木曜日','5' => '金曜日','6' => '土曜日');
        return $ja_list[date('w',strtotime($date))];
    }
}
