<?php
class myYahoo
{
    public static $base_url = "http://jlp.yahooapis.jp/MAService/V1/parse";
    public static $appid = "dj0zaiZpPWlyeURMYVdKTWZvTyZzPWNvbnN1bWVyc2VjcmV0Jng9Yzg-";


    /*
    * Name : getWords
    * Func : 入力された文章を形態素解析し、返却
    */
    public static function analyze($str)
    {
        try
        {
            $str = rawurlencode($str);
            $url = self::$base_url;
            $url .= "?appid=".self::$appid."&sentence=".$str;
            $xml = file_get_contents($url);
            $xml_parse = simplexml_load_string($xml);
            $word_list = $xml_parse->ma_result->word_list;
            
            return $word_list;
        }
        catch (exception $e)
        {
            echo $e;
            exit;
        }
    }
}