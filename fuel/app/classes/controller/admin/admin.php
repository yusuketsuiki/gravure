<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Admin extends Controller_Admin
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('admin/admin/index'));
        parent::$_view->set('title', '管理者一覧 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
	}


    //-----------------------------------
    // 登録／編集
    //-----------------------------------
    public function action_add($id = null)
	{
        $post = Input::post();
        $error_flag = false;

        // validate
        if (!empty($post['mode']))
        {
            $val = Model_Validate_Admin::validate('add');
            if (!$val->run())
            {
                parent::$_view->set_global('error', $val->error());
                $error_flag = true;
            }
        }

        // 初期オブジェクトの生成
        if (!empty($post['id']))
        {
            $id = $post['id'];
        }
        $admin = Model_Admin::forge();

        if ($id)
        {
            $admin = Model_Admin::find($id);
            if (!($admin instanceof Model_Admin))
            {
                $this->action_404();
            }

            // 古いパスワードが正しいか
            if ((!empty($post['mode'])) && ($post['mode'] == 'confirm'))
            {
                if (hash_hmac('sha256',$post['old_password'], false) != $admin->password)
                {
                    parent::$_view->set_global('custom_error', array("古いパスワードが正しくありません。"));
                    $error_flag = true;
                }
            }
        }
        
        // データをオブジェクトにセット
        if (!empty($post))
        {
            $admin->name = $post['name'];
            $admin->password = $post['password'];
        }

        // モードに応じて処理を変える
        if ((empty($post['mode'])) || $error_flag)
        {
            parent::$_view->set('content', ViewModel::forge('admin/admin/add')->set('admin', $admin));
        }
        elseif ($post['mode'] == 'confirm')    // 確認
        {
            parent::$_view->set('content', ViewModel::forge('admin/admin/confirm')->set('admin',$admin));
        }
        elseif ($post['mode'] == 'do')    // 確認
        {
            // 登録
            $admin->password = hash_hmac('sha256',$admin->password, false);
            $admin->delete_flag = "0";
            $admin->rkey = "";
            $admin->save();

            parent::$_view->set('content', ViewModel::forge('admin/admin/do')->set('admin',$admin));
        }
    
        parent::$_view->set('title', '管理者登録 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
	}


    //-----------------------------------
    // 削除
    //-----------------------------------
    public function action_delete($id = null)
	{
        $admin = Model_Admin::find($id);
        if ($admin instanceof Model_Admin)
        {
            $admin->delete_flag ="X";
            $admin->save();
        }

        Response::redirect('admin/admin/');
    }
}
