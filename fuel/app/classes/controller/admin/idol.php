<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Idol extends Controller_Admin
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('admin/idol/index'));
        parent::$_view->set('title', 'アイドル一覧 | アイドル画像まとめサービス 管理画面');

        return parent::$_view;
	}


    //-----------------------------------
    // 画像の編集
    //-----------------------------------
    public function action_image($id = null)
	{
        $idol = Model_Idol::find($id);
        if (!($idol instanceof Model_Idol))
        {
            throw new HttpNotFoundException();
        }

        parent::$_view->set('content', ViewModel::forge('admin/idol/image')->set('idol',$idol));
        parent::$_view->set('title', $idol->name.' | アイドル画像まとめサービス 管理画面');
        return parent::$_view;
    }


    //-----------------------------------
    // 画像の削除
    //-----------------------------------
    public function action_delphoto()
	{
        $post = Input::post();
        $id = $post["id"];
    
        $photo = Model_Photo::find($id);
        if (!($photo instanceof Model_Photo))
        {
            return "false";
        }
        
        // アイドルを取得
        $idol = Model_Idol::find($photo->idol_id);
        $photo->delete();

        // 削除チェック
        $photo = Model_Photo::find($id);
        if (!($photo instanceof Model_Photo))
        {
            // 画像枚数更新
            if ($idol instanceof Model_Idol)
            {
                $idol->updatePictureNum();
            }
        
            return "true";
        }
        return "false";
    }



    //-----------------------------------
    // 画像テーブルの表示
    //-----------------------------------
    public function action_load_photo_table($id = null)
	{
        $idol = Model_Idol::find($id);
        if (!($idol instanceof Model_Idol))
        {
            throw new HttpNotFoundException();
        }
        
        $view = View::forge("admin/common/layout_none");

        $view->set('content', ViewModel::forge('admin/idol/loadphototable')->set('idol',$idol));
        return $view;
    }
}
