<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Shop extends Controller_Admin
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('admin/shop/index'));
        parent::$_view->set('title', '店舗一覧 | コワーキングスペースまとめサービス 管理画面');

        return parent::$_view;
	}


    //-----------------------------------
    // 登録／編集
    //-----------------------------------
    public function action_add($id = null)
	{
        $post = Input::post();
        $file = Input::file();
        $error_flag = false;
        $filedata = array();
        $image_id = null;

        // validate
        if (!empty($post['mode']))
        {
            $val = Model_Validate_Shop::validate('add');
            if (!$val->run())
            {
                parent::$_view->set_global('error', $val->error());
                $error_flag = true;
            }

            // ファイルアップロード
            if ((!empty($file['file1']['name'])) || (!empty($post['filedata'])))
            {
                if ($post['mode'] == 'confirm')
                {
                    $filedata = myImage::doConfirm("shop/tmp/");
                    if (!empty($filedata['error']))
                    {
                        parent::$_view->set_global('custom_error', array($filedata['error']));
                        $error_flag = true;
                    }
                }
                elseif ($post['mode'] == 'do')
                {
                    // ファイル最終処理
                    $image_id = myImage::doDo($post['filedata']);
                }
            }
        }

        // 初期オブジェクトの生成
        if (!empty($post['id']))
        {
            $id = $post['id'];
        }
        $shop = Model_Shop::forge();

        if ($id)
        {
            $shop = Model_Shop::find($id);
            if (!($shop instanceof Model_Shop))
            {
                throw new HttpNotFoundException();
            }
        }
        
        // データをオブジェクトにセット
        if (!empty($post))
        {
            $shop->name = $post['name'];
            $shop->kana = $post['kana'];
            $shop->area_id = null;
            $shop->tdhk_id = $post['tdhk_id'];
            $shop->company_id = $post['company_id'];
            if (!empty($post['city_id']))
            {
                $shop->city_id = $post['city_id'];
            }
            $shop->caption = $post['caption'];
            $shop->url = $post['url'];
            $shop->blog = $post['blog'];
            $shop->twitter = $post['twitter'];
            $shop->facebook = $post['facebook'];
            $shop->address = $post['address'];
            $shop->access = $post['access'];
            $shop->car_space = $post['car_space'];
            $shop->tel = $post['tel'];
            $shop->fax = $post['fax'];
            $shop->open_time = $post['open_time'];
            $shop->holiday = $post['holiday'];
            $shop->capacity = $post['capacity'];
            $shop->establish = $post['establish'];

            if ($image_id)
            {
                $shop->image_id = $image_id;
            }
            $shop->etc = $post['etc'];
            $shop->price = $post['price'];
            $shop->body = $post['body'];
            $shop->speciality = $post['speciality'];
            $shop->equipment = $post['equipment'];
            $shop->delete_flag = "0";
        }

        // モードに応じて処理を変える
        if ((empty($post['mode'])) || $error_flag)
        {
            parent::$_view->set('content', ViewModel::forge('admin/shop/add')->set('shop', $shop));
        }
        elseif ($post['mode'] == 'confirm')    // 確認
        {
            parent::$_view->set('content', ViewModel::forge('admin/shop/confirm')->set('shop',$shop)->set('filedata',$filedata));
        }
        elseif ($post['mode'] == 'do')    // 確認
        {
            $shop->area_id = $post['area_id'];
            $shop->save();
            
            parent::$_view->set('content', ViewModel::forge('admin/shop/do')->set('shop',$shop));
        }
    
        parent::$_view->set('title', '店舗登録 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
	}


    //-----------------------------------
    // 時間帯登録／編集
    //-----------------------------------
    public function action_time($id = null)
	{
        $post = Input::post();
        $error_flag = false;

        if (empty($id) && $post['id'])
        {
            $id = $post['id'];
        }

        // IDの精査
        $shop = Model_Shop::find($id);
        if (!($shop instanceof Model_Shop))
        {
            throw new HttpNotFoundException();
        }
        
        // 予約システム未契約
        if ($shop->type == "0")
        {
            throw new HttpNotFoundException();
        }

        // validate
        if (!empty($post['mode']))
        {
            $val = Model_Validate_Time::validate('add');
            if (!$val->run())
            {
                parent::$_view->set_global('error', $val->error());
                $error_flag = true;
            }
            
            // 時間帯の不正
            $custom_error_list = array();
            $start = $post['start_hour'].$post['start_minute'];
            $end = $post['end_hour'].$post['end_minute'];
            if ($start >= $end)
            {
                $custom_error_list[] = "時間帯の指定が不正です。";
            }
            
            // 時間帯の被り
            $query= Model_Time::find()->where("shop_id","=",$id)->where("delete_flag","=","0");
            if (($shop->type == 'Y') && (!empty($post['youbi'])))
            {
                $query->where('youbi','=',$post['youbi']);
            }
            elseif ($shop->type == 'D')
            {
                $query->where('date','=',$post['year'].$post['month'].$post['day']);
            }
            $query->and_where_open();
            $query->where_open()->where("start","<=",$start)->where('end',">",$start)->where_close();
            $query->or_where_open()->where("start","<",$end)->where('end',">=",$end)->or_where_close();
            $query->or_where_open()->where("start",">",$start)->where('end',"<",$end)->or_where_close();
            
            $time = $query->and_where_close()->get_one();
            
            if ($time instanceof Model_Time)
            {
                $custom_error_list[] = "時間帯が他の時間帯と重なっています。";
            }
            
            if (count($custom_error_list) > 0)
            {
                parent::$_view->set_global('custom_error', $custom_error_list);
                $error_flag = true;
            }
        }

        $time = Model_Time::forge();

        // データをオブジェクトにセット
        if (!empty($post))
        {
            $time->shop_id = $id;
            if (!empty($post['youbi']))
            {
                $time->youbi = $post['youbi'];
            }
            if ($shop->type == 'D')
            {
                $time->date = $post['year'].$post['month'].$post['day'];
            }
            $time->start = $post['start_hour'].$post['start_minute'];
            $time->end = $post['end_hour'].$post['end_minute'];
            $time->money = $post['money'];
            $time->delete_flag = "0";

            if (($post['mode'] == 'do') && (!$error_flag))
            {
                $time->save();

                // 時間帯、料金を登録
                $shop->save_time_price();
                
                Response::redirect('admin/shop/time/'.$id);
            }
        }

        parent::$_view->set('content', ViewModel::forge('admin/shop/time')->set('shop',$shop)->set('time',$time));
        parent::$_view->set('title', '時間帯登録 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
	}


    //-----------------------------------
    // 画像登録
    //-----------------------------------
    public function action_image($id = null)
	{
        $post = Input::post();
        $file = Input::file();
        $error_flag = false;
        $image_id = null;

        if (!empty($post['id']))
        {
            $id = $post['id'];
        }
        $shop = Model_Shop::find($id);
        if (!($shop instanceof Model_Shop))
        {
            throw new HttpNotFoundException();
        }

        // validate
        if (!empty($post['mode']))
        {
            // ファイルアップロード
            if (!empty($file['file1']['name']))
            {
                if ($post['mode'] == 'do')
                {
                    $filedata = myImage::doConfirm("shop/tmp/");
                    if (!empty($filedata['error']))
                    {
                        parent::$_view->set_global('custom_error', array($filedata['error']));
                        $error_flag = true;
                    }
                    
                    if (!$error_flag)
                    {
                        $image_id = myImage::doDo($filedata["image"]);
                    }
                }
            }
        }

        // データをオブジェクトにセット
        if (!empty($image_id))
        {
            // セーブ
            $shopimage = Model_Shopimage::forge();
            $shopimage->image_id = $image_id;
            $shopimage->shop_id = $shop->id;
            $shopimage->type = $post['type'];
            $shopimage->delete_flag = "0";
            $shopimage->save();
        }

        parent::$_view->set('content', ViewModel::forge('admin/shop/image')->set('shop',$shop));
        parent::$_view->set('title', '画像登録 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
    }


    //-----------------------------------
    // 店舗削除
    //-----------------------------------
    public function action_delete($id = null)
	{
        $shop = Model_Shop::find($id);
        if ($shop instanceof Model_Shop)
        {
            $shop->delete_flag ="X";
            $shop->save();
        }

        Response::redirect('admin/shop/');
    }


    //-----------------------------------
    // 時間削除
    //-----------------------------------
    public function action_timedelete($id = null,$shop_id = null)
	{
        $time = Model_Time::find($id);
        if ($time instanceof Model_Time)
        {
            $time->delete();
        }

        $shop = Model_Shop::find()->where("id","=",$shop_id)->get_one();
        if ($shop instanceof Model_Shop)
        {
            $shop->save_time_price();
        }


        Response::redirect('admin/shop/time/'.$shop_id);
    }


    //-----------------------------------
    // 画像削除
    //-----------------------------------
    public function action_imagedelete($id = null)
	{
        $shop = Model_Shop::find($id);
        if ($shop instanceof Model_Shop)
        {
            if ($shop->image_id)
            {
                $image = $shop->image;
                myImage::doDelete($image);

                $shop->image_id = null;
                $shop->save();
                $image->delete();
            }
        }

        Response::redirect('admin/shop/add/'.$id);
    }


    //-----------------------------------
    // 画像削除
    //-----------------------------------
    public function action_shopimagedelete($id = null)
	{
        $shopimage = Model_Shopimage::find($id);
        $shop_id = $shopimage->shop_id;
        if ($shopimage instanceof Model_Shopimage)
        {
            $image = $shopimage->image;
            myImage::doDelete($image);

            $image->delete();
            $shopimage->delete();
        }

        Response::redirect('admin/shop/image/'.$shop_id);
    }
}
