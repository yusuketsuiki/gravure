<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Login extends Controller_Admin
{
    //-----------------------------------
    // ログインページを表示
    //-----------------------------------
    public function action_index()
	{
        $post = Input::post();
        $errors = array();

        // validate
        if (!empty($post['mode']))
        {
            $val = Model_Validate_Admin::validate('add');
            if(!$val->run())
            {
                parent::$_view->set_global('error', $val->error());
            }
            
            // ログインの検証
            if (($post['mode'] == 'do') && (!$val->show_errors()))
            {
                echo hash_hmac('sha256',$this->_salt.$post["password"], false);
                $admin = Model_Admin::find()->where("delete_flag" ,"=","0")->where('name', "=", $post['name'])->where('password','=',hash_hmac('sha256',$this->_salt.$post["password"], false))->get_one();
                 if (!($admin instanceof Model_Admin))
                 {
                    parent::$_view->set_global('custom_error', array("ログインIDかパスワードが誤っています。"));
                 }
                 else
                 {
                    // ログインを実行
                    $rkey = myUtil::getRandomString(32);
                    $admin->rkey = $rkey;
                    $admin->save();
                    
                    // クッキーセット
                    Cookie::set("admin_rkey",$rkey);
                    
                    // リダイレクト
                    Response::redirect('admin/');
                 }
            }
        }

        parent::$_view->set('content', ViewModel::forge('admin/login/index'));
        return parent::$_view;
	}


    //-----------------------------------
    // ログアウト
    //-----------------------------------
    public function action_logout($id = null)
	{
        if (self::$_admin instanceof Model_Admin)
        {
            self::$_admin->rkey = "";
            self::$_admin->save();
        }
        Cookie::set("admin_rkey","");
        
        // リダイレクト
       Response::redirect('admin/login/');
	}
}
