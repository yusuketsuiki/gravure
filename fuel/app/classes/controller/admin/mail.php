<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Mail extends Controller_Admin
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('admin/mail/index'));
        parent::$_view->set('title', 'メール一覧 | コワーキングスペースまとめサービス 管理画面');

        return parent::$_view;
	}


    //-----------------------------------
    // 登録／編集
    //-----------------------------------
    public function action_add($id = null)
	{
        $post = Input::post();
        $error_flag = false;

        // validate
        if (!empty($post['mode']))
        {
            $val = Model_Validate_Mail::validate('add');
            if (!$val->run())
            {
                parent::$_view->set_global('error', $val->error());
                $error_flag = true;
            }
        }

        // 初期オブジェクトの生成
        if (!empty($post['id']))
        {
            $id = $post['id'];
        }
        $mail = Model_Mail::forge();

        if ($id)
        {
            $mail = Model_Mail::find($id);
            if (!($mail instanceof Model_Mail))
            {
                $this->action_404();
            }
        }
        
        // データをオブジェクトにセット
        if (!empty($post))
        {
            $mail->name = $post['name'];
            $mail->subject = $post['subject'];
            $mail->body = $post['body'];
            $mail->delete_flag = "0";
        }

        // モードに応じて処理を変える
        if ((empty($post['mode'])) || $error_flag)
        {
            parent::$_view->set('content', ViewModel::forge('admin/mail/add')->set('mail', $mail));
        }
        elseif ($post['mode'] == 'confirm')    // 確認
        {
            parent::$_view->set('content', ViewModel::forge('admin/mail/confirm')->set('mail',$mail));
        }
        elseif ($post['mode'] == 'do')    // 確認
        {
            $mail->save();
            

            parent::$_view->set('content', ViewModel::forge('admin/mail/do')->set('mail',$mail));
        }
    
        parent::$_view->set('title', '店舗登録 | コワーキングスペースまとめサービス 管理画面');
        return parent::$_view;
	}


    //-----------------------------------
    // 削除
    //-----------------------------------
    public function action_delete($id = null)
	{
        $mail = Model_Mail::find($id);
        if ($mail instanceof Model_Mail)
        {
            $mail->delete_flag ="X";
            $mail->save();
        }

        Response::redirect('admin/mail/');
    }
}
