<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin_Home extends Controller_Admin
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('admin/home/index'));
        return parent::$_view;
	}
}
