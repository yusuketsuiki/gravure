<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Admin extends Controller
{
    public static $_view = "";
    public static $_admin = "";
    public $_salt = "mon#jara";
    
    public function before()
    {
        // レイアウトの共通部分を設定する
        self::$_view = View::forge("admin/common/layout");
        self::$_view->set('navi', View::forge("admin/common/navi"));
        self::$_view->set('title', '画像まとめサービス 管理画面');
        
        // ログインセッションの検証
        $exclude_list = array(
            array('controller' => 'Controller_Admin_Login', 'action' => 'index'),
            array('controller' => 'Controller_Admin_Admin', 'action' => 'index'),
            array('controller' => 'Controller_Admin_Admin', 'action' => 'add'),
        );
        $is_check = true;
        foreach ($exclude_list as $exclude)
        {
            if ((Request::main()->controller == $exclude['controller']) && (Request::main()->action == $exclude['action']))
            {
                $is_check = false;
                break;
            }
        }

        if ($is_check)
        {
            $rkey = Cookie::get("admin_rkey");
            $flg = false;
            if ($rkey)
            {
                self::$_admin =  Model_Admin::find()->where("rkey","=",$rkey)->get_one();
                if (self::$_admin instanceof Model_Admin)
                {
                    $flg = true;
                }
            }
            
            if (!$flg)
            {
                 Response::redirect('admin/login/');
            }
        }
    }
   
     /**
	 * The 404 action for the application.
	 *
	 * @access  public
	 * @return  Response
	 */
	public function action_404()
	{
		return Response::forge(ViewModel::forge('frontend/404'), 404);
	}
}
