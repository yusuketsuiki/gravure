<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Frontendsecure extends Controller_Frontend
{
    public static $_user = "";
    
    public function before()
    {
        parent::before();
        
        // ユーザを取得
        $session_key = Cookie::get('session_key');
        if (!empty($session_key))
        {
            $session = Model_Session::find()->where('session_key',"=",$session_key)->get_one();
            if ($session instanceof Model_Session)
            {
                self::$_user = $session->user;
            }
        }
        
        // ログインしていない
        if (!(self::$_user instanceof Model_User))
        {
            Response::redirect('/');
        }
    }
}
