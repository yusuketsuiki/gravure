<?php
/**
 * Fuel is a fast, lightweight, community driven PHP5 framework.
 *
 * @package    Fuel
 * @version    1.5
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2013 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Frontend extends Controller
{
    public static $_view = "";
    protected $_site_id = "";
    protected $_site_name = "";
    
    public function before()
    {
        // URLに応じてサイトを判定
        $url_list = array();
        if ($_SERVER["SERVER_ADDR"] == "127.0.0.1")
        {
            $url_list = array(
                'gravure' => array("site_id" => 1,"site_name" => "Flickerで見るグラビアアイドル"),
                'gravure2' => array("site_id" => 2,"site_name" => "Flickerで見るAKBグループ"),
                'gravure3' => array("site_id" => 3,"site_name" => "Flickerで見るスターダストアイドル"),
                
            );
        }
        elseif ($_SERVER["SERVER_ADDR"] == "133.242.204.222")
        {
            $url_list = array(
                'gf.yusuketsuiki.com' => array("site_id" => 1,"site_name" => "Flickerで見るグラビアアイドル"),
                'gf2.yusuketsuiki.com' => array("site_id" => 2,"site_name" => "Flickerで見るAKBグループ"),
                'gf3.yusuketsuiki.com' => array("site_id" => 3,"site_name" => "Flickerで見るスターダストアイドル"),
                
            );
        }
        
        foreach ($url_list as $url => $data)
        {
            if ($_SERVER["SERVER_NAME"] == $url)
            {
                $this->_site_id = $data["site_id"];
                $this->_site_name = $data["site_name"];
            }
        }
        if (empty($this->_site_id))
        {
            $this->_site_id = 1;
        }
        
        // レイアウトの共通部分を設定する
        self::$_view = View::forge("frontend/common/layout");
        self::$_view->set('head', View::forge("frontend/common/head")->set('title',$this->_site_name));
//        self::$_view->set('navi', View::forge("frontend/common/navi"));
        self::$_view->set('header', View::forge("frontend/common/header")->set('site_name',$this->_site_name));
        self::$_view->set('sidebar', View::forge("frontend/common/sidebar"));
        self::$_view->set('footer', View::forge("frontend/common/footer"));
        
        self::$_view->set('pankuzu',array());
    }
    
    // レイアウトなしのテンプレートを取得
    public function get_none()
    {
        self::$_view = View::forge("frontend/common/none");
    }
}
