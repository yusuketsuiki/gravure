<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Frontend_Idol extends Controller_Frontend
{
    //-----------------------------------
    // ユーザ詳細
    //-----------------------------------
    public function action_index()
	{
        $id = $this->param('id');

        // 映画を取得
        $idol = Model_Idol::find($id);
        if (!($idol instanceof Model_Idol))
        {
            throw new HttpNotFoundException();
        }

        parent::$_view->set('content', ViewModel::forge('frontend/idol/index')->set('idol',$idol));
        parent::$_view->head->set('title',$idol->name.' | '.$this->_site_name);
        return parent::$_view;
	}
}
