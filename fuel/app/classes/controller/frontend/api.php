<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Frontend_Api extends Controller_None
{
    //-----------------------------------
    // 都道府県IDから市区町村を返却
    //-----------------------------------
    public function action_city()
	{
        $id  = $this->param('id');
        $city_id  = $this->param('city_id');

        if ((empty($id)) || ($id < 1) || ($id > 47))
        {
            exit;
        }

        parent::$_view->set('content', ViewModel::forge('frontend/api/city')->set('id' ,$id)->set("city_id",$city_id));
        return parent::$_view;
	}
}
