<?php
/**
 * @package    Fuel
 * @version    1.5
 * @author     Yusuke Tsuiki
 * @license    MIT License
 * @copyright  2013 Novita.Inc
 * @link       http://
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */
class Controller_Frontend_Home extends Controller_Frontend
{
    //-----------------------------------
    // トップページを表示
    //-----------------------------------
    public function action_index()
	{
        parent::$_view->set('content', ViewModel::forge('frontend/home/index')->set("site_id",$this->_site_id)->set("site_name",$this->_site_name));
        return parent::$_view;
	}
}
