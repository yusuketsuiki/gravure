<?php
class myValidation
{
    public static function _validation_kana($str)
    {
        $ret = true;
        if(!(preg_match("/^[ァ-ヾ]+$/u",$str)))
        {
            $ret = false;
        }
        return $ret;
    }
}
