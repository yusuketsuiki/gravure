<?php
return array(
	'_root_'  => 'frontend/home/index',  // The default route
	'_404_'   => '404/index',    // The main 404 route
    'idol(/:id)' => 'frontend/idol/index',

    // 静的ページ
//    'user' => 'home/user',

    // admin
    'admin' => 'admin/home',

    // company
//    'company' => 'company/home',
);