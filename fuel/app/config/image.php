<?php
     return array(
         //ドライバの指定（使用可能ドライバはgd, imagemagick or imagick）
         'driver' => 'gd',
        //背景色のセット（nullの場合透明色がセット）
         'bgcolor' => null,
        //画質のセット
         'quality' => 100
     );