<?php
return array(
	'version' => 
	array(
		'app' => 
		array(
			'default' => 
			array(
				0 => '001_create_admins',
				1 => '002_create_shops',
				2 => '003_create_tdhks',
				3 => '004_create_tdhks_2',
				4 => '005_create_tdhks_3',
				5 => '006_add_area_id_to_tdhks',
				6 => '007_create_areas',
				7 => '008_create_cities',
				8 => '009_add_city_id_to_shops',
				9 => '010_create_times',
				10 => '011_create_reserves',
				11 => '012_create_images',
				12 => '013_add_image_id_to_shops',
				13 => '014_add_extension_to_images',
				14 => '015_add_path_to_images',
				15 => '016_add_name_to_images',
				16 => '017_drop_path1_to_images',
				17 => '018_drop_path2_to_images',
				18 => '019_drop_path3_to_images',
				19 => '020_drop_url1_to_images',
				20 => '021_drop_url2_to_images',
				21 => '022_drop_url3_to_images',
				22 => '023_create_images_2',
			),
		),
		'module' => 
		array(
		),
		'package' => 
		array(
		),
	),
	'folder' => 'migrations/',
	'table' => 'migration',
);
