<div class="container" id="mainContent">
    <h2>メール登録</h2>
    
    <?php echo Form::open("admin/mail/add") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">メール名</th>
            <td><?php echo htmlspecialchars($mail->name) ?></td>
        </tr>
        <tr>
            <th class="form_th">題名</th>
            <td><?php echo htmlspecialchars($mail->subject) ?></td>
        </tr>
        <tr>
            <th class="form_th">本文</th>
            <td><?php echo nl2br(htmlspecialchars($mail->body)) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　登　録　') ?></div>

    <?php echo Form::hidden('id',$mail->id) ?>
    <?php echo Form::hidden('name',$mail->name) ?>
    <?php echo Form::hidden('subject',$mail->subject) ?>
    <?php echo Form::hidden('body',$mail->body) ?>
    <?php echo Form::hidden('mode','do') ?>
</div>