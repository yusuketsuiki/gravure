<div class="container" id="mainContent">
    <h2>メール登録</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <?php echo Form::open(array('action' => "admin/mail/add",'enctype' => 'multipart/form-data')) ?>
    <table class="form_table">
        <tr>
            <th class="form_th">メール名</th>
            <td><?php echo Form::input('name', $mail->name, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">題名</th>
            <td><?php echo Form::input('subject', $mail->subject, array('class' => 'text-long')) ?></td>
            </td>
        </tr>
        <tr>
            <th class="form_th">本文</th>
            <td><?php echo Form::textarea('body', $mail->body, array('class' => 'textarea-long')) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　確　認　') ?></div>
    
    <?php echo Form::hidden('id', $mail->id) ?>
    <?php echo Form::hidden('mode','confirm') ?>
    <?php echo Form::close() ?>
</div>
