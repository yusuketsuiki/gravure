<div class="container" id="mainContent">
    <h2>ログイン</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <?php echo Form::open("admin/login") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">ログインID</th>
            <td><?php echo Form::input('name', '', array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">パスワード</th>
            <td><?php echo Form::password('password', '', array('class' => 'text-middle')) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　ログイン　') ?></div>
    
    <?php echo Form::hidden('mode','do') ?>
    <?php echo Form::close() ?>
</div>