<div class="container" id="mainContent">
    <h2>店舗登録</h2>
    
    <?php echo Form::open("admin/shop/add") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">店舗名<span class="form_require">*</span></th>
            <td><?php echo htmlspecialchars($shop->name) ?></td>
        </tr>
        <tr>
            <th class="form_th">店舗名カナ</th>
            <td><?php echo htmlspecialchars($shop->kana) ?></td>
        </tr>
        <tr>
            <th class="form_th">メイン画像</th>
            <td>
                <?php if (!empty($filedata['image'])): ?>
                    <img src="<?php echo $filedata['image']->get_url1()?>" />
                <?php elseif ($shop->image_id): ?>
                    <img src="<?php echo $shop->image->get_url1()?>" />
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th class="form_th">会社<span class="form_require">*</span></th>
            <td><?php echo htmlspecialchars($shop->company->name) ?></td>
        </tr>
        <tr>
            <th class="form_th">都道府県／市区町村<span class="form_require">*</span></th>
            <td><?php echo htmlspecialchars($shop->tdhk->name) ?> <?php echo htmlspecialchars($shop->city->name) ?></td>
        </tr>
        <tr>
            <th class="form_th">住所(番地以下)<span class="form_require">*</span></th>
            <td><?php echo htmlspecialchars($shop->address) ?></td>
        </tr>
        <tr>
            <th class="form_th">最寄り駅<span class="form_require">*</span></th>
            <td><?php echo nl2br(htmlspecialchars($shop->access)) ?></td>
        </tr>
        <tr>
            <th class="form_th">電話番号</th>
            <td><?php echo htmlspecialchars($shop->tel) ?></td>
        </tr>
        <tr>
            <th class="form_th">FAX</th>
            <td><?php echo htmlspecialchars($shop->fax) ?></td>
        </tr>
        <tr>
            <th class="form_th">営業時間<span class="form_require">*</span></th>
            <td><?php echo htmlspecialchars($shop->open_time) ?></td>
        </tr>
        <tr>
            <th class="form_th">定休日</th>
            <td><?php echo htmlspecialchars($shop->holiday) ?></td>
        </tr>
        <tr>
            <th class="form_th">キャプション</th>
            <td><?php echo htmlspecialchars($shop->caption) ?></td>
        </tr>
        <tr>
            <th class="form_th">駐車場</th>
            <td><?php echo htmlspecialchars($shop->car_space) ?></td>
        </tr>
        <tr>
            <th class="form_th">ホームページ</th>
            <td><?php echo htmlspecialchars($shop->url) ?></td>
        </tr>
        <tr>
            <th class="form_th">Blog</th>
            <td><?php echo htmlspecialchars($shop->blog) ?></td>
        </tr>
        <tr>
            <th class="form_th">Twitter</th>
            <td><?php echo htmlspecialchars($shop->twitter) ?></td>
        </tr>
        <tr>
            <th class="form_th">Facebook</th>
            <td><?php echo htmlspecialchars($shop->facebook) ?></td>
        </tr>
        <tr>
            <th class="form_th">席数</th>
            <td><?php echo htmlspecialchars($shop->capacity) ?>席</td>
        </tr>
        <tr>
            <th class="form_th">設立日</th>
            <td><?php echo htmlspecialchars($shop->establish) ?></td>
        </tr>
        <tr>
            <th class="form_th">説明</th>
            <td><?php echo nl2br(htmlspecialchars($shop->body)) ?></td>
        </tr>
        <tr>
            <th class="form_th">料金</th>
            <td><?php echo nl2br(htmlspecialchars($shop->price)) ?></td>
        </tr>
        <tr>
            <th class="form_th">特徴</th>
            <td><?php echo nl2br(htmlspecialchars($shop->speciality)) ?></td>
        </tr>
        <tr>
            <th class="form_th">設備</th>
            <td><?php echo nl2br(htmlspecialchars($shop->equipment)) ?></td>
        </tr>
        <tr>
            <th class="form_th">備考</th>
            <td><?php echo nl2br(htmlspecialchars($shop->etc)) ?></td>
        </tr>
    </table>

    <div class="form_button"><?php echo Form::submit('submit', '　登　録　') ?></div>

    <?php echo Form::hidden('id',$shop->id) ?>
    <?php echo Form::hidden('name',$shop->name) ?>
    <?php echo Form::hidden('kana',$shop->kana) ?>
    <?php echo Form::hidden('area_id',$shop->tdhk->area_id) ?>
    <?php echo Form::hidden('company_id',$shop->company_id) ?>
    <?php echo Form::hidden('tdhk_id',$shop->tdhk_id) ?>
    <?php echo Form::hidden('city_id',$shop->city_id) ?>
    <?php echo Form::hidden('address',$shop->address) ?>
    <?php echo Form::hidden('access',$shop->access) ?>
    <?php echo Form::hidden('tel',$shop->tel) ?>
    <?php echo Form::hidden('fax',$shop->fax) ?>
    <?php echo Form::hidden('open_time',$shop->open_time) ?>
    <?php echo Form::hidden('holiday',$shop->holiday) ?>
    <?php echo Form::hidden('caption',$shop->caption) ?>
    <?php echo Form::hidden('car_space',$shop->car_space) ?>
    <?php echo Form::hidden('url',$shop->url) ?>
    <?php echo Form::hidden('blog',$shop->blog) ?>
    <?php echo Form::hidden('twitter',$shop->twitter) ?>
    <?php echo Form::hidden('facebook',$shop->facebook) ?>
    <?php echo Form::hidden('capacity',$shop->capacity) ?>
    <?php echo Form::hidden('establish',$shop->establish) ?>
    <?php echo Form::hidden('body',$shop->body) ?>
    <?php echo Form::hidden('price',$shop->price) ?>
    <?php echo Form::hidden('speciality',$shop->speciality) ?>
    <?php echo Form::hidden('equipment',$shop->equipment) ?>
    <?php echo Form::hidden('etc',$shop->etc) ?>
    <?php echo Form::hidden('mode','do') ?>
    
    <?php if (!empty($filedata['image'])):?>
    <?php echo Form::hidden('filedata[path]',$filedata['image']->path) ?>
    <?php echo Form::hidden('filedata[name]',$filedata['image']->name) ?>
    <?php echo Form::hidden('filedata[extension]',$filedata['image']->extension) ?>
    <?php endif ?>
    <?php echo Form::close() ?>
</div>