<div class="container" id="mainContent">
    <h2>店舗登録</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <?php echo Form::open(array('action' => "admin/shop/add",'enctype' => 'multipart/form-data')) ?>
    <table class="form_table">
        <tr>
            <th class="form_th">店舗名<span class="form_require">*</span></th>
            <td><?php echo Form::input('name', $shop->name, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">店舗名カナ</th>
            <td><?php echo Form::input('kana', $shop->kana, array('class' => 'text-middle')) ?></td>
        </tr>
        <?php /*?>
        <!--
        <?php if ($shop->id > 0):?>
        <tr>
            <th class="form_th">画像登録</th>
            <td><?php echo Html::anchor("admin/shop/image/".$shop->id, "画像登録") ?></td>
        </tr>
        <?php endif ?>
        -->
        <?php */ ?>
        <tr>
            <th class="form_th">メイン画像</th>
            <td>
                <?php if ($shop->image_id): ?>
                    <img src="<?php echo $shop->image->get_url1()?>" /><br />
                    <?php echo Html::anchor("admin/shop/imagedelete/".$shop->id, "画像を削除する") ?>
                <?php else: ?>
                    <?php echo Form::file('file1'); ?>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th class="form_th">会社<span class="form_require">*</span></th>
            <td>
                <?php echo Form::select('company_id',$shop->company_id,$company_list); ?>
            </td>
        </tr>
        <tr>
            <th class="form_th">都道府県／市区町村<span class="form_require">*</span></th>
            <td>
                <?php echo Form::select('tdhk_id',$shop->tdhk_id,$tdhk_list,array("onchange" => "get_city('".$shop->city_id."')")); ?>
                <span id="city_area"></span>
            </td>
        </tr>
        <tr>
            <th class="form_th">住所(番地以下)<span class="form_require">*</span></th>
            <td><?php echo Form::input('address', $shop->address, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">最寄り駅<span class="form_require">*</span></th>
            <td><?php echo Form::textarea('access', $shop->access, array('class' => 'textarea-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">電話番号</th>
            <td><?php echo Form::input('tel', $shop->tel, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">FAX</th>
            <td><?php echo Form::input('fax', $shop->fax, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">営業時間<span class="form_require">*</span></th>
            <td><?php echo Form::input('open_time', $shop->open_time, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">定休日</th>
            <td><?php echo Form::input('holiday', $shop->holiday, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">キャプション</th>
            <td><?php echo Form::input('caption', $shop->caption, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">駐車場</th>
            <td><?php echo Form::input('car_space', $shop->car_space, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">ホームページ</th>
            <td><?php echo Form::input('url', $shop->url, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">Blog</th>
            <td><?php echo Form::input('blog', $shop->blog, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">Twitter</th>
            <td><?php echo Form::input('twitter', $shop->twitter, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">Facebook</th>
            <td><?php echo Form::input('facebook', $shop->facebook, array('class' => 'text-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">席数</th>
            <td><?php echo Form::input('capacity', $shop->capacity, array('class' => 'text-short')) ?>席</td>
        </tr>
        <tr>
            <th class="form_th">設立日</th>
            <td><?php echo Form::input('establish', $shop->establish, array('class' => 'text-middle')) ?></td>
        </tr>
        <tr>
            <th class="form_th">説明</th>
            <td><?php echo Form::textarea('body', $shop->body, array('class' => 'textarea-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">料金</th>
            <td><?php echo Form::textarea('price', $shop->price, array('class' => 'textarea-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">特徴</th>
            <td><?php echo Form::textarea('speciality', $shop->speciality, array('class' => 'textarea-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">設備</th>
            <td><?php echo Form::textarea('equipment', $shop->equipment, array('class' => 'textarea-long')) ?></td>
        </tr>
        <tr>
            <th class="form_th">備考</th>
            <td><?php echo Form::textarea('etc', $shop->etc, array('class' => 'textarea-long')) ?></td>
        </tr>
    </table>


    <div class="form_button"><?php echo Form::submit('submit', '　確　認　') ?></div>
    
    <?php echo Form::hidden('id', $shop->id) ?>
    <?php echo Form::hidden('mode','confirm') ?>
    <?php echo Form::close() ?>
</div>

<script type="text/javascript">
function get_city(city_id)
{
    var tdhk_id = $("#form_tdhk_id").val();
    if (city_id)
    {
        $("#city_area").load("/api/city/" + tdhk_id + "/" + city_id);
    }
    else
    {
        $("#city_area").load("/api/city/" + tdhk_id);
    }
}
window.onload = get_city('<?php echo $shop->city_id?>');
</script>
