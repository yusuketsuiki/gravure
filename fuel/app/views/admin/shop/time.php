<div class="container" id="mainContent">
    <h2>時間帯登録</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <?php echo Form::open("admin/shop/time") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">店名</th>
            <td>
                <?php echo Html::anchor("admin/shop/add/" . $shop->id, htmlspecialchars($shop->name)) ?>
            </td>
        </tr>
        <tr>
            <th class="form_th">登録種別</th>
            <td>
                <?php echo $shop->type_str() ?>
            </td>
        </tr>
        <?php if ($shop->type == 'Y'):?>
        <tr>
            <th class="form_th">曜日</th>
            <td>
                <?php echo Form::radio("youbi","1") ?>日曜日&nbsp;
                <?php echo Form::radio("youbi","2") ?>月曜日&nbsp;
                <?php echo Form::radio("youbi","3") ?>火曜日&nbsp;
                <?php echo Form::radio("youbi","4") ?>水曜日&nbsp;
                <?php echo Form::radio("youbi","5") ?>木曜日&nbsp;
                <?php echo Form::radio("youbi","6") ?>金曜日&nbsp;
                <?php echo Form::radio("youbi","7") ?>土曜日&nbsp;
            </td>
        </tr>
        <?php elseif ($shop->type == 'D'):?>
        <tr>
            <th class="form_th">日にち</th>
            <td>
                <?php echo Form::select('year', "", $ymd_list['year_list']) ?>年
                <?php echo Form::select('month', "", $ymd_list['month_list']) ?>月
                <?php echo Form::select('day', "", $ymd_list['day_list']) ?>日
            </td>
        </tr>
        <?php endif ?>
        <tr>
            <th class="form_th">時間帯</th>
            <td>
                <?php echo Form::select('start_hour', substr($time->start,0,2), $ymd_list['hour_list']) ?>時<?php echo Form::select('start_minute',  substr($time->start,2,2), $ymd_list['min_list']) ?>分
                〜
                <?php echo Form::select('end_hour',  substr($time->end,0,2), $ymd_list['hour_list']) ?>時<?php echo Form::select('end_minute',  substr($time->end,2,2), $ymd_list['min_list']) ?>分
            </td>
        </tr>
        <tr>
            <th class="form_th">料金</th>
            <td><?php echo Form::input('money',  $time->money, array('class' => 'text-middle')) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　追　加　') ?></div>
    <?php echo Form::hidden('id', $shop->id) ?>
    <?php echo Form::hidden('mode','do') ?>
    <?php echo Form::close() ?>


    <?php if (count($times) > 0):?>
    <h2>登録済の時間帯</h2>
    <table class="listing-middle" cellpadding="0" cellspacing="0">
        <tr class="table_title">
            <?php if ($shop->type == 'Y'):?>
            <th>曜日</th>
            <?php elseif ($shop->type == 'D'):?>
            <th>日にち</th>
            <?php endif ?>
            <th>時間帯</th>
            <th>料金</th>
            <th class="table_menu">削除</th>
        </tr>
        
        <?php foreach ($times as $time):?>
        <tr>
            <?php if ($shop->type == 'Y'):?>
            <td><?php echo $youbi_list[$time->youbi] ?></td>
            <?php elseif ($shop->type == 'D'):?>
            <td><?php echo substr($time->date,0,4)."年".substr($time->date,4,2)."月".substr($time->date,6,2)."日" ?></td>
            <?php endif ?>
            <td>
                <?php echo substr($time->start,0,2) ?>時<?php echo substr($time->start,2,4) ?>分
                〜
                <?php echo substr($time->end,0,2) ?>時<?php echo substr($time->end,2,4) ?>分
            </td>
            <td><?php echo number_format($time->money)?>円</td>
            <td class="table_menu"><?php echo Html::anchor("admin/shop/timedelete/".$time->id."/".$time->shop_id,"削除")?></td>
        </tr>
        <?php endforeach ?>
    </table>
    <?php endif ?>


</div>


