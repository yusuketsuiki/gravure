<div class="header" id="pageHeader">画像まとめサイト 管理画面</div>
<div class="nav" id="navContent">
    <p class="navTitle"><u>アイドル管理</u></p>
    <p class="navText"><?php echo Html::anchor("admin","トップページ")?></p>
    <p class="navText"><?php echo Html::anchor("admin/idol","アイドル一覧")?></p>

    <p class="navTitle"><u>その他メニュー</u></p>
    <p class="navText"><?php echo Html::anchor("admin/admin","管理者管理")?></p>
    <p class="navText"><?php echo Html::anchor("admin/login/logout","ログアウト")?></p>
</div>
