<div class="container" id="mainContent">
    <h2>管理者登録</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <?php echo Form::open("admin/admin/add") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">ログインID</th>
            <td><?php echo Form::input('name', $admin->name, array('class' => 'text-middle')) ?></td>
        </tr>
        <?php if (!empty($admin['id'])): ?>
        <tr>
            <th class="form_th">古いパスワード</th>
            <td><?php echo Form::password('old_password', '', array('class' => 'text-middle')) ?></td>
        </tr>
        <?php endif ?>
        <tr>
            <th class="form_th">パスワード</th>
            <td><?php echo Form::password('password', '', array('class' => 'text-middle')) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　確　認　') ?></div>
    
    <?php echo Form::hidden('id', $admin->id) ?>
    <?php echo Form::hidden('mode','confirm') ?>
    <?php echo Form::close() ?>
</div>