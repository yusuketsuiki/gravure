<div class="container" id="mainContent">
    <h2>管理者登録</h2>

    <?php echo Form::open("admin/admin/add") ?>
    <table class="form_table">
        <tr>
            <th class="form_th">ログインID</th>
            <td><?php echo htmlspecialchars($admin->name) ?></td>
        </tr>
        <tr>
            <th class="form_th">パスワード</th>
            <td><?php echo htmlspecialchars($admin->password) ?></td>
        </tr>
    </table>
    <div class="form_button"><?php echo Form::submit('submit', '　登　録　') ?></div>
    
    <?php echo Form::hidden('id',$admin->id) ?>
    <?php echo Form::hidden('name',$admin->name) ?>
    <?php echo Form::hidden('password',$admin->password) ?>
    <?php echo Form::hidden('mode','do') ?>
    <?php echo Form::close() ?>
</div>