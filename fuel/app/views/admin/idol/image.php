<div class="container" id="mainContent">
    <h2><?php echo h($idol->name)?>画像一覧</h2>
    <?php echo View::forge('admin/error'); ?>
    
    <div id="table_area"></div>
</div>

<script type="text/javascript">
function delete_photo(id)
{
    var url = "/admin/idol/delphoto/";
    $.post(url, {id : id}).done(function(data) {
        if (data == "true")
        {
            load_table(<?php echo $idol->id?>);
        }
        else
        {
            alert("画像の削除に失敗");
        }
    });
}

// テーブルをロード
function load_table(idol_id)
{
    var url = "/admin/idol/load_photo_table/" + idol_id;
    $("#table_area").load(url);
}

window.onload = function()
{
    load_table(<?php echo $idol->id?>);
}
</script>