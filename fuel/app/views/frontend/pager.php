<?php
  $page_list = $pager->getPageList();
  $link = $pager->getRule();
  $options = $pager->getOptions();
?>
<?php if($pager->getTotalPage() > 1):?>
<div class="pagination">
    <ul>
        <?php if ($pager->getPage() > 1):?>
            <li><?php echo Html::anchor($link."?p=".($pager->getPage() - 1).$options, "Prev") ?></li>
        <?php endif ?>

        <?php foreach ($page_list as $i => $v): ?>
            <li><?php echo Html::anchor($link."?p=".$v.$options, $v) ?></li>
        <?php endforeach ?>

        <?php if ($pager->getPage() < $pager->getTotalPage()):?>
            <li><?php echo Html::anchor($link."?p=".($pager->getPage() + 1).$options, "Next") ?></li>
        <?php endif ?>
    </ul>
</div>
<?php endif ?>