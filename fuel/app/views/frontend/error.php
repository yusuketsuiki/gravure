<?php if ((!empty($error)) || (!empty($custom_error))): ?>
    <div id="error_area">
        <ul>
            <?php if (!empty($error)): ?>
                <?php foreach ($error as $e): ?>
                    <li><?php echo $e->get_message() ?></li>
                <?php endforeach ?>
            <?php endif ?>

            <?php if (!empty($custom_error)): ?>
                <?php foreach ($custom_error as $e): ?>
                    <li><?php echo $e ?></li>
                <?php endforeach ?>
            <?php endif ?>
        </ul>
    </div>
<?php endif ?>