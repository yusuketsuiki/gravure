<?php
    $i = 0; // カウンタ
    $cols = 6;
?>

<h2><?php echo htmlspecialchars($idol->name)?></h2>
<table>
<?php foreach ($photo_list as $photo):?>
    <?php if ($i % $cols == 0):?><tr><?php endif ?>
        <td style="text-align:center">
            <a href="<?php echo $photo->get_page()?>" target="_blank"><img src="<?php echo $photo->get_q_150()?>" /></a>
        </td>
    <?php if ($i % $cols == ($cols - 1)):?></tr><?php endif ?>
    <?php $i++ ?>
<?php endforeach ?>

<?php if ($i % $cols != 0):?></tr><?php endif ?>

</table>

<?php echo View::forge('frontend/pager')->set('pager',$pager); ?>