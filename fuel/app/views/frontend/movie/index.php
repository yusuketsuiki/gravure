<script type="text/javascript" src="/js/highcharts_wrap.js"></script>
<div class="container-fluid">
<div class="row-fluid">
    <div class="row-fluid">
        <div class="block span12">
            <a href="#tablewidget" class="block-heading" data-toggle="collapse"><?php echo htmlspecialchars($movie->name)?></a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="block span6">
            <p class="block-heading">「<?php echo htmlspecialchars($movie->name)?>」の主な指標</p>

            <table class="table list">
              <tbody>
                  <tr>
                      <td><p>投稿数</p></td>
                      <td><?php echo $movie->total?></td>
                  </tr>
                  <tr>
                      <td><p>平均点</p></td>
                      <td><?php echo round($movie->avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p><a id="mania_icon" style="text-decoration:underline">マニア</a>の平均点</p></td>
                      <td><?php echo round($movie->mania_avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p><a id="fun_icon" style="text-decoration:underline">ファン</a>の平均点</p></td>
                      <td><?php echo round($movie->fun_avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p><a id="user_icon" style="text-decoration:underline">一般ユーザ</a>の平均点</p></td>
                      <td><?php echo round($movie->user_avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p><a id="ichigen_icon" style="text-decoration:underline">一見さん</a>の平均点</p></td>
                      <td><?php echo round($movie->ichigen_avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p>一見さん＋<a id="h_ichigen_icon" style="text-decoration:underline">非公開ユーザ</a>の平均点</p></td>
                      <td><?php echo round($movie->sakura_avg,2)?></td>
                  </tr>
                  <tr>
                      <td><p>一見さん率 <i id="ichigen_perc" class="icon-question-sign"></i></p></td>
                      <td><?php echo round($movie->ichigen_perc,2)?>%</td>
                  </tr>
                  <tr>
                      <td><p>一見さん＋非公開ユーザ率 <i id="h_ichigen_perc" class="icon-question-sign"></i></p></td>
                      <td><?php echo round($movie->sakura_perc,2)?>%</td>
                  </tr>
                  <tr>
                      <td><p>ファン指数 <i title="この指数が高いほど、一見さんが高評価を投稿し、全体の評価に影響を与えている可能性があります(指数の算出方法はアバウト参照)" id="g_fun_index" class="icon-question-sign"></i></p></td>
                      <td><?php echo round($movie->sakura_index,2)?></td>
                  </tr>
                  <tr>
                      <td><p>アンチ指数 <i title="この指数が高いほど、一見さんが低評価を投稿し、全体の評価に影響を与えている可能性があります(指数の算出方法はアバウト参照)" id="arashi_index" class="icon-question-sign"></i></p></td>
                      <td><?php echo round($movie->arashi_index,2)?></td>
                  </tr>
              </tbody>
            </table>
        </div>

        <div class="block span6">
            <p class="block-heading">平均点</p>

            <div class="block-body">
            <div id="avg_graff_area"></div>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="block span6">
            <p class="block-heading">レビュワー</p>

            <div class="block-body">
                <div id="review_graff_area"></div>
    
                <input type="radio" name="review" value="A" onclick="draw_review_graff()" checked>&nbsp;全て&nbsp;
                <?php for ($i = 5 ; $i >= 1; $i--):?>
                <input type="radio" name="review" value="<?php echo $i?>" onclick="draw_review_graff()">&nbsp;<?php echo $i?>点&nbsp;
                <?php endfor ?>
            </div>
        </div>

        <div class="block span6">
            <p class="block-heading">得点</p>

            <div class="block-body">
                <div id="point_graff_area"></div>

                <input type="radio" name="point" value="A" onclick="draw_point_graff()" checked>&nbsp;全て&nbsp;
                <?php foreach ($column_hash as $key => $value):?>
                <input type="radio" name="point" value="<?php echo $key?>" onclick="draw_point_graff()">&nbsp;<?php echo $value?>&nbsp;
                <?php if ($key == '2'):?><br /><?php endif ?>
                <?php endforeach ?>
                <input type="radio" name="point" value="X0" onclick="draw_point_graff()">&nbsp;一見さん＋レビュー数非公開&nbsp;
            </div>
        </div>
    </div>


</div>
</div>





<script type="text/javascript">
function draw_graff()
{
    draw_chart("avg_graff_area",JSON.parse('<?php echo $avg_graff ?>'));
}

// 得点グラフを描画
function draw_point_graff()
{
    var graff_type = $('input[name="point"]:checked').val();
    var url = "/movie/point/<?php echo $movie->id?>/" + graff_type;
    
    // Ajaxでデータを取得
    $.get(url,'',
        function(data)
        {
            draw_pie("point_graff_area",JSON.parse(data));
        }
    );
    
}

// レビュアーグラフを描画
function draw_review_graff()
{
    var graff_type = $('input[name="review"]:checked').val();
    var url = "/movie/review/<?php echo $movie->id?>/" + graff_type;
    
    // Ajaxでデータを取得
    $.get(url,'',
        function(data)
        {
            draw_pie("review_graff_area",JSON.parse(data));
        }
    );
    
}

$(function() {
    $('#mania_icon').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "100件以上のレビューを投稿しているレビュアー"
    });

    $('#fun_icon').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "10件〜99件のレビューを投稿しているレビュアー"
    });

    $('#user_icon').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "2件〜9件のレビューを投稿しているレビュアー"
    });

    $('#ichigen_icon').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "1件だけレビューを投稿しているレビュアー"
    });

    $('#h_ichigen_icon').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "一見さん＋レビュー投稿数非公開のレビュアーの平均点"
    });

    $('#ichigen_perc').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "レビューを投稿しているユーザのうち、一見さんは何%か（非公開のユーザは除く）"
    });

    $('#h_ichigen_perc').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "レビューを投稿しているユーザのうち、一見さん＋非公開ユーザは何%か"
    });

    $('#g_fun_index').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "レビューを投稿しているユーザのうち、一見さん＋非公開ユーザは何%か"
    });

    $('#arashi_index').tipsy({
        gravity: 'w', //どこに向かって出るか s,n,w,e
        fade: true, //フェードさせるか
        fallback: "レビューを投稿しているユーザのうち、一見さん＋非公開ユーザは何%か"
    });

});

// ページがロードされた後に、グラフを出力する
draw_point_graff();
draw_review_graff();
draw_graff();
</script>
