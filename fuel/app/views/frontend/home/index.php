<?php
    $i = 0; // カウンタ
    $cols = 6;
?>
<h1><?php echo htmlspecialchars($site_name)?></h1>

<h2>人気のアイドル</h2>
<table>
<?php foreach ($photo_list as $photo):?>
    <?php if ($i % $cols == 0):?><tr><?php endif ?>
        <td style="text-align:center">
            <?php echo Html::anchor('idol/'.$photo->idol_id, '<img src="'.$photo->get_q_150().'" />') ?><br />
            <?php echo htmlspecialchars($photo->idol->name) ?>
        </td>
    <?php if ($i % $cols == ($cols - 1)):?></tr><?php endif ?>
    <?php $i++ ?>
<?php endforeach ?>

<?php if ($i % $cols != 0):?></tr><?php endif ?>

</table>