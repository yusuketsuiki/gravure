<?php
    $year = substr($month,0,4);
    $mon = substr($month,4,2);

    // 最終日
    $last_day = 31;
    if (($mon == "04") || ($mon == "06") || ($mon == "09") || ($mon == "11"))
    {
        $last_day = 30;
    }
    elseif ($mon == "02")
    {
        if ($year % 400 == 0)
        {
            $last_day = 29;
        }
        elseif ($year % 100 == 0)
        {
            $last_day = 28;
        }
        elseif ($year % 4 == 0)
        {
            $last_day = 29;
        }
        else
        {
            $last_day = 28;
        }
    }
    
    // 最初の曜日
    $first_youbi = date('w',strtotime($month."01"));
    
    // 予約できる日を取得
    $time_day_list = array();
    $youbi_list = array();

    if ($shop->type == 'Y')
    {
        $youbi_list = array();
        $times = Model_Time::find()->where('shop_id','=',$shop->id)->order_by('youbi')->get();
        foreach ($times as $time)
        {
            $youbi_list[$time->youbi - 1] = true;
        }
    }
    elseif ($shop->type == 'D')
    {
        $times = Model_Time::find()->where('shop_id','=',$shop->id)->where('date', 'like', $month."%")->order_by('date')->get();
        foreach ($times as $time)
        {
            $time_day_list[$time->date] = true;
        }
    }
    
    for ($i = 1; $i <= $last_day; $i++)
    {
        $day = myUtil::addZeroDay($i);
        
        if ( ($shop->type == 'A') || (($shop->type == 'Y') && (!empty($youbi_list[date('w',strtotime($month.$day))]))) )
        {
            $time_day_list[$month.$day] = true;
        }
    }

?>

<!-- ▽カレンダー▽-->
<?php echo substr($month,0,4)?>年<?php echo substr($month,4,2)?>月<br />
<table>
    <tr>
        <!-- ▽空白▽ -->
        <?php for($i = 0; $i < $first_youbi; $i++):?>
        <td style="border:1px solid black;">　</td>
        <?php endfor ?>
        <!-- △空白△ -->
        
        <?php for ($day = 1; $day <= $last_day; $day++):?>
            <?php if (($day > 1) && ($first_youbi % 7 == 0)):?>
            <tr>
            <?php endif ?>
            <td style="border:1px solid black;">
                <?php if (!empty($time_day_list[$month.myUtil::addZeroDay($day)])):?>
                <?php echo Html::anchor("shop/reserve/".$shop->id."/".$month."/".$month.myUtil::addZeroDay($day) , myUtil::addZeroDay($day)) ?>
                <?php else :?>
                <?php echo myUtil::addZeroDay($day) ?>
                <?php endif ?>
            </td>
            <?php if ($first_youbi % 7 == 6):?>
            </tr>
            <?php endif ?>
        
            <?php $first_youbi++ ?>
        <?php endfor ?>
    <?php if ($first_youbi % 7 != 0) :?>
    </tr>
    <?php endif ?>
</table>
<!-- △カレンダー△-->
