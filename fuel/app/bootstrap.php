<?php
//define ('SITE_NAME','ムービー・コンフィデンシャル - 映画レビューの分析サービス');


// Load in the Autoloader
require COREPATH.'classes'.DIRECTORY_SEPARATOR.'autoloader.php';
class_alias('Fuel\\Core\\Autoloader', 'Autoloader');

include 'HTTP/OAuth/Consumer.php';

// Bootstrap the framework DO NOT edit this
require COREPATH.'bootstrap.php';


Autoloader::add_classes(array(
    'myUtil' => APPPATH.'classes/myUtil.php',
    'myImage' => APPPATH.'classes/myImage.php',
    //'myMail' => APPPATH.'classes/myMail.php',
    'myValidation' => APPPATH.'classes/myValidation.php',
    //'myTwitter' => APPPATH.'classes/myTwitter.php',
    'myFlickr' => APPPATH.'classes/myFlickr.php',
    'myMovie' => APPPATH.'classes/myMovie.php',
    'myYahoo' => APPPATH.'classes/myYahoo.php',
    'myConsts' => APPPATH.'classes/myConsts.php',
    // Add classes you want to override here
	// Example: 'View' => APPPATH.'classes/view.php',
));

// Register the autoloader
Autoloader::register();

/**
 * Your environment.  Can be set to any of the following:
 *
 * Fuel::DEVELOPMENT
 * Fuel::TEST
 * Fuel::STAGE
 * Fuel::PRODUCTION
 */
Fuel::$env = (isset($_SERVER['FUEL_ENV']) ? $_SERVER['FUEL_ENV'] : Fuel::DEVELOPMENT);

// Initialize the framework with the config file.
Fuel::init('config.php');

//-----------------------------
// 関数エイリアス
//-----------------------------
function h($str)
{
    return htmlspecialchars($str);
}
